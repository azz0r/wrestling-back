<?php


/*begin API Benchmark*/
$time = explode(' ', microtime());
$apiStartTime = $time[1] + $time[0];


define('_ROOT', 		realpath('../'));
define('BASE_DIR', 		dirname(dirname(__FILE__)));
define('SITE_ID' , 		1);
define('INI_PATH', 		apache_getenv('_INI_PATH_'));
define('DS', 			DIRECTORY_SEPARATOR);
define('LS',			'/');
define('BR',			'<br />');


define('_LIBRARY', 					_ROOT.DS.'library');
define('_APP', 						_ROOT.DS.'application'); 
define('_CONFIG', 					_APP.DS.'configs'); 


define('_CLASS', 					_LIBRARY);
define('_CLASS_APP', 				_APP.DS);


define('_MODULE', 					_APP.DS.'modules');
define('_SCRIPT', 					_LIBRARY.DS.'script');   


define('_LOCAL_PUBLIC', 			_ROOT.DS.'public');


define('CURRENT_URL',				'CURRENT_URL');
define('REFERER', 					isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : NULL);


define('_MODULE_DEFAULT',			'default');
define('_ACTION_DEFAULT',			'index');


define('_DEV', 						FALSE);
define('_LIVE', 					TRUE);
define('_DEBUG', 					_DEV);


defined('_ENV_') || define('_ENV_', (getenv('_ENV_') ? getenv('_ENV_') : 'local'));


require_once(_CONFIG.DS.'init.php');
require_once(_APP.DS.'bootstrap.php');


$bootstrap = new Bootstrap();
$bootstrap->initRest();  // initializing rest as it's first thing which is necessary to handle eventual exceptions
//$bootstrap->initSession();  // uncomment this for MongoDB session handler


try {
	$bootstrap->initLog()
				->initDb()
	 		  	->run();
	 		  
} catch(System_Exception_Application $e) {

	$bootstrap->proccessException($e);
	
} catch(System_Exception_Connect $e) {

	$bootstrap->proccessException($e);
	
} catch(System_Exception_Database $e) {

	$bootstrap->proccessException($e);
	
} catch (PDOException $e) {
	$bootstrap->proccessException($e);
	
} catch (Exception $e) {
	$bootstrap->proccessException($e);
}