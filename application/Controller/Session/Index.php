<?php


class Controller_Session_Index  extends System_Controller
{


    public function get () {

        $sessionId = $this->_getParam('session_id');

        if (!empty($sessionId)) {
            if (isset($_SESSION['logged_in']) && $_SESSION['logged_in'] != 1) {
                return $this->setError(904);
            }
            $sessionId = $_GET['session_id'];
        } else {
            $sessionId = session_id();
        }

        return array('session_id' => $sessionId);
    }


}