<?php
/**
 * User: aaron lote
 * Date: 14/01/2013
 * Time: 13:53
 */


class Controller_Test_Index  extends System_Controller
{


    public function init () {
        $this->skipAuthentication();
        $this->matchId = 1;
    }

    public function importTables() {

        $import = array();

        $import['Championship'][] = array('title' => 'WWE Tag Team Championship');
        $import['Championship'][] = array('title' => 'World Heavyweight Championship');
        $import['Championship'][] = array('title' => 'WWE Championship');
        $import['Championship'][] = array('title' => 'WWE Intercontinental Championship');
        $import['Championship'][] = array('title' => 'WWE United States Championship');
        $import['Championship'][] = array('title' => 'WWE Divas Championship');

        $import['Wrestler'][] = array('alias' => 'Alberto Del Rio');
        $import['Wrestler'][] = array('alias' => 'Wade Barrett');
        $import['Wrestler'][] = array('alias' => 'Antonio Cesaro');
        $import['Wrestler'][] = array('alias' => 'The Rock');
        $import['Wrestler'][] = array('alias' => 'Daniel Bryan');
        $import['Wrestler'][] = array('alias' => 'Kane');
        $import['Wrestler'][] = array('alias' => 'Big Show');
        $import['Wrestler'][] = array('alias' => 'Big E Langston');
        $import['Wrestler'][] = array('alias' => 'Booker T');
        $import['Wrestler'][] = array('alias' => 'Brock Lesnar');
        $import['Wrestler'][] = array('alias' => 'Brodus Clay');
        $import['Wrestler'][] = array('alias' => 'Chris Jericho');
        $import['Wrestler'][] = array('alias' => 'Christian');
        $import['Wrestler'][] = array('alias' => 'CM Punk');
        $import['Wrestler'][] = array('alias' => 'Cody Rhodes');
        $import['Wrestler'][] = array('alias' => 'Damien Sandow');
        $import['Wrestler'][] = array('alias' => 'Darren Young');
        $import['Wrestler'][] = array('alias' => 'David Otunga');
        $import['Wrestler'][] = array('alias' => 'Dean Ambrose');
        $import['Wrestler'][] = array('alias' => 'Dolph Ziggler');
        $import['Wrestler'][] = array('alias' => 'Great Khali');
        $import['Wrestler'][] = array('alias' => 'Jack Swagger');
        $import['Wrestler'][] = array('alias' => 'John Cena');
        $import['Wrestler'][] = array('alias' => 'Justin Gabriel');
        $import['Wrestler'][] = array('alias' => 'Kofi Kingston');
        $import['Wrestler'][] = array('alias' => 'Mark Henry');
        $import['Wrestler'][] = array('alias' => 'The Miz');
        $import['Wrestler'][] = array('alias' => 'Mr. McMahon');
        $import['Wrestler'][] = array('alias' => 'Paul Heyman');
        $import['Wrestler'][] = array('alias' => 'R-Truth');
        $import['Wrestler'][] = array('alias' => 'Randy Orton');
        $import['Wrestler'][] = array('alias' => 'Rey Mysterio');
        $import['Wrestler'][] = array('alias' => 'Ryback');
        $import['Wrestler'][] = array('alias' => 'Santino Marella');
        $import['Wrestler'][] = array('alias' => 'Sheamus');

        $import['Wrestler'][] = array('alias' => 'Roman Reigns');
        $import['Wrestler'][] = array('alias' => 'Seth Rollins');
        $import['Wrestler'][] = array('alias' => 'Dean Ambrose');

        /* rumble */
        $import['Wrestler'][] = array('alias' => 'Goldust');
        $import['Wrestler'][] = array('alias' => 'Bo Dallas');
        $import['Wrestler'][] = array('alias' => 'The Godfather');

        /* 3mb */
        $import['Wrestler'][] = array('alias' => 'Drew McIntrye');
        $import['Wrestler'][] = array('alias' => 'Jinder Mahal');
        $import['Wrestler'][] = array('alias' => 'Heath Slater');

        $import['Wrestler'][] = array('alias' => 'Sin Cara');
        $import['Wrestler'][] = array('alias' => 'Ted Dibiase');
        $import['Wrestler'][] = array('alias' => 'Tensai');
        $import['Wrestler'][] = array('alias' => 'Fandango');
        $import['Wrestler'][] = array('alias' => "Titus O Neil");
        $import['Wrestler'][] = array('alias' => 'Triple H');
        $import['Wrestler'][] = array('alias' => 'Tyson Kidd');
        $import['Wrestler'][] = array('alias' => 'The Undertaker');
        $import['Wrestler'][] = array('alias' => 'William Regal');
        $import['Wrestler'][] = array('alias' => 'Zack Ryder');

        $import['Wrestler'][] = array('alias' => 'Tamina Snuka', 'male' => 0);
        $import['Wrestler'][] = array('alias' => 'Rosa Mendes', 'male' => false);
        $import['Wrestler'][] = array('alias' => 'Vicki Guerrero', 'male' => false);
        $import['Wrestler'][] = array('alias' => 'AJ Lee', 'male' => false);
        $import['Wrestler'][] = array('alias' => 'Aksana', 'male' => false);
        $import['Wrestler'][] = array('alias' => 'Alicia Fox', 'male' => false);
        $import['Wrestler'][] = array('alias' => 'Cameron', 'male' => false);
        $import['Wrestler'][] = array('alias' => 'Kaitlyn', 'male' => false);
        $import['Wrestler'][] = array('alias' => 'Layla', 'male' => false);
        $import['Wrestler'][] = array('alias' => 'Naomi', 'male' => false);
        $import['Wrestler'][] = array('alias' => 'Natalya Hart', 'male' => false);

        foreach ($import as $tableName => $values) {
            $tempDB = $this->mongoDB->{$tableName};
            $tempDB->drop();

            foreach ($values as $subValues) {
                $this->internalCall(array('controller' => 'Mimic_Index', 'method' => 'post',
                    'params' => array('tableName' => $tableName, 'data' => $subValues)));
            }
        }

        $tempDB = $this->mongoDB->Show;
        $tempDB->drop();
        $tempDB = $this->mongoDB->Show_Prediction;
        $tempDB->drop();

        $params = array('from' => 0, 'to' => COUNT_HIGH, 'tableName' => 'Wrestler');
        $wrestlers = $this->internalCall(array('controller' => 'Mimic_Index', 'params' => $params));

        $params = array('from' => 0, 'to' => COUNT_HIGH, 'tableName' => 'Championship');
        $championships = $this->internalCall(array('controller' => 'Mimic_Index', 'params' => $params));

        $this->wrestlersArray = array();

        foreach ($championships as $championship) {
            $this->championshipArray[$championship->id] = $championship;
        }

        foreach ($wrestlers as $wrestler) {
            switch ($wrestler->alias) {
                case 'Alberto Del Rio':
                    $wrestler->championship = $this->championshipArray[2];
                    break;
                case 'Wade Barrett':
                    $wrestler->championship = $this->championshipArray[4];
                    break;
                case 'The Rock':
                    $wrestler->championship = $this->championshipArray[3];
                    break;
                case 'Kane':case 'Daniel Bryan':
                $wrestler->championship = $this->championshipArray[1];
                break;
                case 'Kaitlyn':
                    $wrestler->championship = $this->championshipArray[6];
                    break;
                case 'Antonio Cesaro':
                    $wrestler->championship = $this->championshipArray[5];
                    break;
            }
            if ($wrestler->championship != false) {
                $this->internalCall(array('controller' => 'Mimic_Index', 'method' => 'put','params' => array('tableName' => 'Wrestler', 'data' => $wrestler)));
            }
            $this->wrestlersArray[$wrestler->alias] = $wrestler;
        }

    }


    public function createRoyalRumble2013() {

        $wrestlers      = array();
        $show           = (object) array('title' => 'Royal Rumble (2013)',
            'date' => '2013-01-27', 'location' => 'Phoenix, Arizona',
            'stadium' => 'US Airways Center');

        $wrestlers[1][] = (object) $this->wrestlersArray['Antonio Cesaro'];
        $wrestlers[2][] = (object) $this->wrestlersArray['The Miz'];
        $teams[]        = (object) array('alias' => '', 'wrestlers' => $wrestlers[1]);
        $teams[]        = (object) array('alias' => '', 'wrestlers' => $wrestlers[2]);
        $results        = (object) array('winner' => $this->wrestlersArray['Antonio Cesaro'], 'loser' => $this->wrestlersArray['The Miz'], 'by' => 'pin');
        $show->matches[] = (object) array(
            'id'            => $this->matchId++,
            'type'          => 'single',
            'championship'  =>  $this->championshipArray[5],
            'order'         => 0,
            'teams'         => $teams,
            'results'       => array($results)
        );


        $wrestlers = array(); $teams = array();
        $wrestlers[1][] = (object) $this->wrestlersArray['Alberto Del Rio'];
        $wrestlers[2][] = (object) $this->wrestlersArray['Big Show'];
        $teams[]        = (object) array('alias' => '', 'wrestlers' => $wrestlers[1]);
        $teams[]        = (object) array('alias' => '', 'wrestlers' => $wrestlers[2]);
        $results        = (object) array('winner' => $this->wrestlersArray['Alberto Del Rio'],
            'loser' => $this->wrestlersArray['Big Show'], 'by' => 'ko');
        $show->matches[] = (object) array(
            'id'            => $this->matchId++,
            'type'          => 'single',
            'title'         => 'Last Man Standing',
            'championship'  => $this->championshipArray[2],
            'by'            => 'ko',
            'order'         => 0,
            'teams'         => $teams,
            'results'       => array($results)
        );


        $wrestlers = array(); $teams = array();
        $wrestlers[1][] = (object) $this->wrestlersArray['Daniel Bryan'];
        $wrestlers[1][] = (object) $this->wrestlersArray['Kane'];
        $wrestlers[2][] = (object) $this->wrestlersArray['Cody Rhodes'];
        $wrestlers[2][] = (object) $this->wrestlersArray['Damien Sandow'];
        $teams[]        = (object) array('alias' => 'Team Hell No', 'wrestlers' => $wrestlers[1]);
        $teams[]        = (object) array('alias' => 'Rhodes Scholars', 'wrestlers' => $wrestlers[2]);
        $results        = (object) array('winner' => $this->wrestlersArray['Daniel Bryan'], 'loser' => $this->wrestlersArray['Damien Sandow'], 'by' => 'submission');
        $show->matches[] = (object) array(
            'id'            => $this->matchId++,
            'type'          => 'tag-team',
            'championship'  =>  $this->championshipArray[1],
            'order'         => 0,
            'teams'         => $teams,
            'results'       => array($results)
        );

        //7	The Rock (c) defeated CM Punk (with Paul Heyman)	Singles match for the WWE Championship. If The Rock was disqualified or counted out, he would lose the championship.[10]
        $wrestlers = array(); $teams = array();
        $rock =  clone (object) $this->wrestlersArray['The Rock'];
        $rock->championship = false;
        $punk = clone (object) $this->wrestlersArray['CM Punk'];
        $punk->championship = $this->championshipArray[3];
        $wrestlers[1][] = $rock;
        $wrestlers[2][] = $punk;
        $teams[]        = (object) array('alias' => '', 'wrestlers' => $wrestlers[1]);
        $teams[]        = (object) array('alias' => '', 'wrestlers' => $wrestlers[2]);
        $results        = (object) array('winner' => $rock, 'loser' => $punk, 'by' => 'pin');
        $show->matches[] = (object) array(
            'id'            => $this->matchId++,
            'title'         => 'If The Rock was disqualified or counted out, he would lose the championship',
            'type'          => 'single',
            'championship'  => $this->championshipArray[3],
            'order'         => 0,
            'teams'         => $teams,
            'results'       => array($results)
        );

        $teams = array(); $results = array();
        $teams[] = (object) array('wrestlers' => array($this->wrestlersArray['Dolph Ziggler']));
        $teams[] = (object) array('wrestlers' => array($this->wrestlersArray['Chris Jericho']));
        $teams[] = (object) array('wrestlers' => array($this->wrestlersArray['Cody Rhodes']));
        $teams[] = (object) array('wrestlers' => array($this->wrestlersArray['Kofi Kingston']));
        $teams[] = (object) array('wrestlers' => array($this->wrestlersArray['Santino Marella']));
        $teams[] = (object) array('wrestlers' => array($this->wrestlersArray['Drew McIntrye']));
        $teams[] = (object) array('wrestlers' => array($this->wrestlersArray['Titus O Neil']));
        $teams[] = (object) array('wrestlers' => array($this->wrestlersArray['Goldust']));
        $teams[] = (object) array('wrestlers' => array($this->wrestlersArray['David Otunga']));
        $teams[] = (object) array('wrestlers' => array($this->wrestlersArray['Heath Slater']));
        $teams[] = (object) array('wrestlers' => array($this->wrestlersArray['Sheamus']));
        $teams[] = (object) array('wrestlers' => array($this->wrestlersArray['Tensai']));
        $teams[] = (object) array('wrestlers' => array($this->wrestlersArray['Brodus Clay']));
        $teams[] = (object) array('wrestlers' => array($this->wrestlersArray['Rey Mysterio']));
        $teams[] = (object) array('wrestlers' => array($this->wrestlersArray['Darren Young']));
        $teams[] = (object) array('wrestlers' => array($this->wrestlersArray['Bo Dallas']));
        $teams[] = (object) array('wrestlers' => array($this->wrestlersArray['The Godfather']));
        $teams[] = (object) array('wrestlers' => array($this->wrestlersArray['Wade Barrett']));
        $teams[] = (object) array('wrestlers' => array($this->wrestlersArray['John Cena']));
        $teams[] = (object) array('wrestlers' => array($this->wrestlersArray['Damien Sandow']));
        $teams[] = (object) array('wrestlers' => array($this->wrestlersArray['Daniel Bryan']));
        $teams[] = (object) array('wrestlers' => array($this->wrestlersArray['Antonio Cesaro']));
        $teams[] = (object) array('wrestlers' => array($this->wrestlersArray['Great Khali']));
        $teams[] = (object) array('wrestlers' => array($this->wrestlersArray['Kane']));
        $teams[] = (object) array('wrestlers' => array($this->wrestlersArray['Zack Ryder']));
        $teams[] = (object) array('wrestlers' => array($this->wrestlersArray['Randy Orton']));
        $teams[] = (object) array('wrestlers' => array($this->wrestlersArray['Jinder Mahal']));
        $teams[] = (object) array('wrestlers' => array($this->wrestlersArray['The Miz']));
        $teams[] = (object) array('wrestlers' => array($this->wrestlersArray['Sin Cara']));
        $teams[] = (object) array('wrestlers' => array($this->wrestlersArray['Ryback']));
        $eliminations = array(
            array('loser' => 'Santino Marella', 'winner' => 'Cody Rhodes'),
            array('loser' => 'Drew McIntrye', 'winner' => 'Chris Jericho'),
            array('loser' => 'Titus O Neil', 'winner' => 'Sheamus'),
            array('loser' => 'David Otunga', 'winner' => 'Sheamus'),
            array('loser' => 'Goldust', 'winner' => 'Cody Rhodes'),
            array('loser' => 'Brodus Clay', 'winner' => 'Sheamus'),
            array('loser' => 'Tensai', 'winner' => 'Kofi Kingston'),
            array('loser' => 'Darren Young', 'winner' => 'Kofi Kingston'),
            array('loser' => 'Kofi Kingston', 'winner' => 'Cody Rhodes'),
            array('loser' => 'The Godfather', 'winner' => 'Dolph Ziggler'),
            array('loser' => 'Heath Slater', 'winner' => 'John Cena'),
            
            array('loser' => 'Cody Rhodes', 'winner' => 'John Cena'),
            array('loser' => 'Rey Mysterio', 'winner' => 'Wade Barrett'),
            array('loser' => 'Great Khali', 'winner' => 'Kane'),
            array('loser' => 'Kane', 'winner' => 'Daniel Bryan'),
            array('loser' => 'Daniel Bryan', 'winner' => 'Kane'),
            array('loser' => 'Zack Ryder', 'winner' => 'Randy Orton'),
            array('loser' => 'Antonio Cesaro', 'winner' => 'John Cena'),
            array('loser' => 'Jinder Mahal', 'winner' => 'Sheamus'),
            array('loser' => 'Wade Barrett', 'winner' => 'Bo Dallas'),
            array('loser' => 'Bo Dallas', 'winner' => 'Wade Barrett'),
            array('loser' => 'Damien Sandow', 'winner' => 'Ryback'),
            array('loser' => 'Sin Cara', 'winner' => 'Ryback'),
            array('loser' => 'The Miz', 'winner' => 'Ryback'),
            array('loser' => 'Chris Jericho', 'winner' => 'Dolph Ziggler'),
            array('loser' => 'Randy Orton', 'winner' => 'Ryback'),
            array('loser' => 'Dolph Ziggler', 'winner' => 'Sheamus'),
            array('loser' => 'Sheamus', 'winner' => 'Ryback'),
            array('loser' => 'Ryback', 'winner' => 'John Cena'),
        );
        foreach ($eliminations as $elim) {
            $results[] = (object) array(
                'winner' => $this->wrestlersArray[$elim['winner']],
                'loser' =>  $this->wrestlersArray[$elim['loser']],
                'by' => 'over-the-top-rope'
            );
        }


        $show->matches[] = (object) array(
            'id'            => $this->matchId++,
            'type'          => 'royal-rumble',
            'championship'  => false,
            'eliminations'  => count($results),
            'order'         => 0,
            'teams'         => $teams,
            'results'       => $results
        );

        return $show;
    }


    public function createEliminationChamber2013() {

        $wrestlers      = array();
        $show           = (object) array('title' => 'Elimination Chamber (2013)', 'date' => '2013-02-17', 'location' => 'New Orleans, Louisiana',
            'stadium' => 'New Orleans Arena');


        $wrestlers[1][] = (object) $this->wrestlersArray['Alberto Del Rio'];
        $wrestlers[2][] = (object) $this->wrestlersArray['Big Show'];
        $teams[]        = (object) array('alias' => '', 'wrestlers' => $wrestlers[1]);
        $teams[]        = (object) array('alias' => '', 'wrestlers' => $wrestlers[2]);
        $results        = (object) array('winner' => $this->wrestlersArray['Alberto Del Rio'], 'loser' => $this->wrestlersArray['Big Show'], 'by' => 'submission');
        $show->matches[] = (object) array(
            'id'            => $this->matchId++,
            'type'          => 'single',
            'championship'  =>  $this->championshipArray[2],
            'order'         => 0,
            'teams'         => $teams,
            'results'       => array($results)
        );


        $wrestlers = array(); $teams = array();
        $wrestlers[1][] = (object) $this->wrestlersArray['Antonio Cesaro'];
        $wrestlers[2][] = (object) $this->wrestlersArray['The Miz'];
        $teams[]        = (object) array('alias' => '', 'wrestlers' => $wrestlers[1]);
        $teams[]        = (object) array('alias' => '', 'wrestlers' => $wrestlers[2]);
        $results        = (object) array('winner' => $this->wrestlersArray['Antonio Cesaro'], 'loser' => $this->wrestlersArray['The Miz'], 'by' => 'dq');
        $show->matches[] = (object) array(
            'id'            => $this->matchId++,
            'type'          => 'single',
            'championship'  => $this->championshipArray[5],
            'order'         => 0,
            'teams'         => $teams,
            'results'       => array($results)
        );


        $wrestlers = array(); $teams = array();
        $wrestlers[1][] = (object) $this->wrestlersArray['Dolph Ziggler'];
        $wrestlers[2][] = (object) $this->wrestlersArray['Kofi Kingston'];
        $teams[]        = (object) array('alias' => '', 'wrestlers' => $wrestlers[1]);
        $teams[]        = (object) array('alias' => '', 'wrestlers' => $wrestlers[2]);
        $results        = (object) array('winner' => $this->wrestlersArray['Dolph Ziggler'], 'loser' => $this->wrestlersArray['Kofi Kingston'], 'by' => 'pin');
        $show->matches[] = (object) array(
            'id'            => $this->matchId++,
            'type'          => 'single',
            'championship'  =>  false,
            'order'         => 0,
            'teams'         => $teams,
            'results'       => array($results)
        );


        $wrestlers = array(); $teams = array();
        $wrestlers[1][] = (object) $this->wrestlersArray['Kaitlyn'];
        $wrestlers[2][] = (object) $this->wrestlersArray['Tamina Snuka'];
        $teams[]        = (object) array('alias' => '', 'wrestlers' => $wrestlers[1]);
        $teams[]        = (object) array('alias' => '', 'wrestlers' => $wrestlers[2]);
        $results        = (object) array('winner' => $this->wrestlersArray['Kaitlyn'], 'loser' => $this->wrestlersArray['Tamina Snuka'], 'by' => 'pin');
        $show->matches[] = (object) array(
            'id'            => $this->matchId++,
            'type'          => 'single',
            'championship'  => $this->championshipArray[6],
            'order'         => 0,
            'teams'         => $teams,
            'results'       => array($results)
        );


        $wrestlers = array(); $teams = array();
        $wrestlers[1][] = (object) $this->wrestlersArray['John Cena'];
        $wrestlers[1][] = (object) $this->wrestlersArray['Sheamus'];
        $wrestlers[1][] = (object) $this->wrestlersArray['Ryback'];

        $wrestlers[2][] = (object) $this->wrestlersArray['Roman Reigns'];
        $wrestlers[2][] = (object) $this->wrestlersArray['Dean Ambrose'];
        $wrestlers[2][] = (object) $this->wrestlersArray['Seth Rollins'];
        $teams[]        = (object) array('alias' => '', 'wrestlers' => $wrestlers[1]);
        $teams[]        = (object) array('alias' => 'The Shield', 'wrestlers' => $wrestlers[2]);
        $results        = (object) array('winner' => $this->wrestlersArray['Roman Reigns'], 'loser' => $this->wrestlersArray['Ryback'], 'by' => 'pin');
        $show->matches[] = (object) array(
            'id'            => $this->matchId++,
            'type'          => 'tag-team',
            'championship'  =>  false,
            'order'         => 0,
            'teams'         => $teams,
            'results'       => array($results)
        );


        //7	The Rock (c) defeated CM Punk (with Paul Heyman)	Singles match for the WWE Championship. If The Rock was disqualified or counted out, he would lose the championship.[10]
        $wrestlers = array(); $teams = array();
        $teams[]        = (object) array('alias' => '', 'wrestlers' => array((object) $this->wrestlersArray['The Rock']));
        $teams[]        = (object) array('alias' => '', 'wrestlers' => array((object) $this->wrestlersArray['CM Punk']));
        $results        = (object) array('winner' => $this->wrestlersArray['The Rock'], 'loser' => (object) $this->wrestlersArray['CM Punk'], 'by' => 'pin');
        $show->matches[] = (object) array(
            'id'            => $this->matchId++,
            'title'         => 'If The Rock was disqualified or counted out, he would lose the championship',
            'type'          => 'single',
            'championship'  => $this->championshipArray[3],
            'order'         => 0,
            'teams'         => $teams,
            'results'       => array($results)
        );


        $wrestlers = array(); $teams = array(); $results = array();
        $wrestlers[1][] = (object) $this->wrestlersArray['Daniel Bryan'];
        $wrestlers[2][] = (object) $this->wrestlersArray['Mark Henry'];
        $wrestlers[3][] = (object) $this->wrestlersArray['Kane'];
        $wrestlers[4][] = (object) $this->wrestlersArray['Chris Jericho'];
        $wrestlers[5][] = (object) $this->wrestlersArray['Randy Orton'];
        $wrestlers[6][] = (object) $this->wrestlersArray['Jack Swagger'];
        $teams[]        = (object) array('alias' => '', 'wrestlers' => $wrestlers[1]);
        $teams[]        = (object) array('alias' => '', 'wrestlers' => $wrestlers[2]);
        $teams[]        = (object) array('alias' => '', 'wrestlers' => $wrestlers[3]);
        $teams[]        = (object) array('alias' => '', 'wrestlers' => $wrestlers[4]);
        $teams[]        = (object) array('alias' => '', 'wrestlers' => $wrestlers[5]);
        $teams[]        = (object) array('alias' => '', 'wrestlers' => $wrestlers[6]);
        $results[]        = (object) array('winner' => $this->wrestlersArray['Mark Henry'], 'loser' => $this->wrestlersArray['Daniel Bryan'], 'by' => 'pin');
        $results[]        = (object) array('winner' => $this->wrestlersArray['Mark Henry'], 'loser' => $this->wrestlersArray['Kane'], 'by' => 'pin');
        $results[]        = (object) array('winner' => $this->wrestlersArray['Randy Orton'], 'loser' => $this->wrestlersArray['Mark Henry'], 'by' => 'pin');
        $results[]        = (object) array('winner' => $this->wrestlersArray['Chris Jericho'], 'loser' => $this->wrestlersArray['Randy Orton'], 'by' => 'pin');
        $results[]        = (object) array('winner' => $this->wrestlersArray['Jack Swagger'], 'loser' => $this->wrestlersArray['Randy Orton'], 'by' => 'pin');
        $show->matches[] = (object) array(
            'id'            => $this->matchId++,
            'type'          => 'elimination-chamber',
            'championship'  => false,
            'eliminations'  => count($results),
            'order'         => 0,
            'teams'         => $teams,
            'results'       => $results
        );


        return $show;
    }

    public function createWrestlemania29() {


        $wrestlers = array(); $teams = array();
        $show      = (object) array('title' => 'Wrestlemania 29', 'date' => '2013-03-07', 'location' => 'NYC', 'stadium' => 'Met Life Stadium');

        $wrestlers[1][] = (object) $this->wrestlersArray['Daniel Bryan'];
        $wrestlers[1][] = (object) $this->wrestlersArray['Kane'];
        $wrestlers[2][] = (object) $this->wrestlersArray['Dolph Ziggler'];
        $wrestlers[2][] = (object) $this->wrestlersArray['Big E Langston'];
        $teams[]        = (object) array('alias' => 'Team Hell No', 'wrestlers' => $wrestlers[1]);
        $teams[]        = (object) array('alias' => 'Rhodes Scholars', 'wrestlers' => $wrestlers[2]);
        $show->matches[] = (object) array(
            'id'            => $this->matchId++,
            'type'          => 'tag-team',
            'championship'  =>  $this->championshipArray[1],
            'order'         => 0,
            'teams'         => $teams,
            'results'       => false
        );

        /* second show */
        $wrestlers = array(); $teams = array();
        $wrestlers[1][] = (object) $this->wrestlersArray['The Rock'];
        $wrestlers[2][] = (object) $this->wrestlersArray['John Cena'];
        $teams[]        = (object) array('alias' => false, 'wrestlers' => $wrestlers[1]);
        $teams[]        = (object) array('alias' => false, 'wrestlers' => $wrestlers[2]);
        $results        = (object) array('winner' => $this->wrestlersArray['John Cena'], 'loser' => $this->wrestlersArray['The Rock'], 'by' => 'pin');

        $show->matches[] = (object) array(
            'id'            => $this->matchId++,
            'type'          => 'single',
            'title'         => 'Twice in a lifetime',
            'championship'  => $this->championshipArray[3],
            'order'         => $this->matchId++,
            'teams'         => $teams,
            'results'       => array($results)
        );

        $wrestlers = array(); $teams = array();
        $wrestlers[1][] = (object) $this->wrestlersArray['Alberto Del Rio'];
        $wrestlers[2][] = (object) $this->wrestlersArray['Jack Swagger'];
        $teams[]        = (object) array('alias' => false, 'wrestlers' => $wrestlers[1]);
        $teams[]        = (object) array('alias' => false, 'wrestlers' => $wrestlers[2]);
        $results        = (object) array('winner' => $this->wrestlersArray['Alberto Del Rio'], 'loser' => $this->wrestlersArray['Jack Swagger'], 'by' => 'submission');

        $show->matches[] = (object) array(
            'id'            => $this->matchId++,
            'type'          => 'single',
            'title'         => '',
            'championship'  => $this->championshipArray[2],
            'order'         => $this->matchId++,
            'teams'         => $teams,
            'results'       => array($results)
        );


        $wrestlers = array(); $teams = array();
        $wrestlers[1][] = (object) $this->wrestlersArray['Mark Henry'];
        $wrestlers[2][] = (object) $this->wrestlersArray['Ryback'];
        $teams[]        = (object) array('alias' => false, 'wrestlers' => $wrestlers[1]);
        $teams[]        = (object) array('alias' => false, 'wrestlers' => $wrestlers[2]);
        $results        = (object) array('winner' => $this->wrestlersArray['Mark Henry'], 'loser' => $this->wrestlersArray['Ryback'], 'by' => 'pin');

        $show->matches[] = (object) array(
            'id'            => $this->matchId++,
            'type'          => 'single',
            'title'         => 'Battle of the Heavyweights',
            'championship'  => false,
            'order'         => $this->matchId++,
            'teams'         => $teams,
            'results'       => array($results)
        );


        $wrestlers = array(); $teams = array();
        $wrestlers[1][] = (object) $this->wrestlersArray['The Undertaker'];
        $wrestlers[2][] = (object) $this->wrestlersArray['CM Punk'];
        $teams[]        = (object) array('alias' => false, 'wrestlers' => $wrestlers[1]);
        $teams[]        = (object) array('alias' => false, 'wrestlers' => $wrestlers[2]);
        $results        = (object) array('winner' => $this->wrestlersArray['The Undertaker'], 'loser' => $this->wrestlersArray['CM Punk'], 'by' => 'pin');
        $show->matches[] = (object) array(
            'id'            => $this->matchId++,
            'type'          => 'single',
            'title'         => 'The Streak',
            'championship'  => false,
            'order'         => $this->matchId++,
            'teams'         => $teams,
            'results'       => array($results)
        );


        $wrestlers = array(); $teams = array();
        $wrestlers[1][] = (object) $this->wrestlersArray['Triple H'];
        $wrestlers[2][] = (object) $this->wrestlersArray['Brock Lesnar'];
        $teams[]        = (object) array('alias' => false, 'wrestlers' => $wrestlers[1]);
        $teams[]        = (object) array('alias' => false, 'wrestlers' => $wrestlers[2]);
        $results        = (object) array('winner' => $this->wrestlersArray['Triple H'], 'loser' => $this->wrestlersArray['Brock Lesnar'], 'by' => 'pin');
        $show->matches[] = (object) array(
            'id'            => $this->matchId++,
            'type'          => 'single',
            'title'         => 'No Holds Barred Match; If Lesnar wins, Triple H must retire',
            'championship'  => false,
            'order'         => $this->matchId++,
            'teams'         => $teams,
            'results'       => array($results)
        );

        $wrestlers = array(); $teams = array();
        $wrestlers[1][] = (object) $this->wrestlersArray['Wade Barrett'];
        $wrestlers[2][] = (object) $this->wrestlersArray['The Miz'];
        $teams[]        = (object) array('alias' => false, 'wrestlers' => $wrestlers[1]);
        $teams[]        = (object) array('alias' => false, 'wrestlers' => $wrestlers[2]);
        $results        = (object) array('winner' => $this->wrestlersArray['The Miz'], 'loser' => $this->wrestlersArray['Wade Barrett'], 'by' => 'submission');
        $show->matches[] = (object) array(
            'id'            => $this->matchId++,
            'type'          => 'single',
            'title'         => '',
            'championship'  => $this->championshipArray[4],
            'order'         => $this->matchId++,
            'teams'         => $teams,
            'results'       => array($results)
        );


        $wrestlers = array(); $teams = array();
        $wrestlers[1][] = (object) $this->wrestlersArray['Chris Jericho'];
        $wrestlers[2][] = (object) $this->wrestlersArray['Fandango'];
        $teams[]        = (object) array('alias' => false, 'wrestlers' => $wrestlers[1]);
        $teams[]        = (object) array('alias' => false, 'wrestlers' => $wrestlers[2]);
        $results        = (object) array('winner' => $this->wrestlersArray['Fandango'], 'loser' => $this->wrestlersArray['Chris Jericho'], 'by' => 'pin');
        $show->matches[] = (object) array(
            'id'            => $this->matchId++,
            'type'          => 'single',
            'title'         => '',
            'championship'  => false,
            'order'         => $this->matchId++,
            'teams'         => $teams,
            'results'       => array($results)
        );


        $wrestlers = array(); $teams = array();
        $wrestlers[1][] = (object) $this->wrestlersArray['Big Show'];
        $wrestlers[1][] = (object) $this->wrestlersArray['Sheamus'];
        $wrestlers[1][] = (object) $this->wrestlersArray['Randy Orton'];

        $wrestlers[2][] = (object) $this->wrestlersArray['Roman Reigns'];
        $wrestlers[2][] = (object) $this->wrestlersArray['Dean Ambrose'];
        $wrestlers[2][] = (object) $this->wrestlersArray['Seth Rollins'];
        $teams[]        = (object) array('alias' => '', 'wrestlers' => $wrestlers[1]);
        $teams[]        = (object) array('alias' => 'The Shield', 'wrestlers' => $wrestlers[2]);
        $results        = (object) array('winner' => $this->wrestlersArray['Dean Ambrose'], 'loser' => $this->wrestlersArray['Randy Orton'], 'by' => 'pin');

        $show->matches[] = (object) array(
            'id'            => $this->matchId++,
            'type'          => 'tag-team',
            'championship'  =>  false,
            'order'         => 0,
            'teams'         => $teams,
            'results'       => array($results)
        );


        return $show;
    }


    public function createShow() {

        $show = $this->createRoyalRumble2013();
        $show->id   = 1;
        $show       = $this->model->get($show, 'Show');
        $c          = $this->mongoDB->Show;
        $c->update(array('title' => $show->title), $show, array('upsert' => true, 'fsync' => true));

        $show = $this->createEliminationChamber2013();
        $show->id   = 2;
        $show       = $this->model->get($show, 'Show');
        $c          = $this->mongoDB->Show;
        $c->update(array('title' => $show->title), $show, array('upsert' => true, 'fsync' => true));


        $show = $this->createWrestlemania29();
        $show->id   = 3;
        $show       = $this->model->get($show, 'Show');
        $c          = $this->mongoDB->Show;
        $c->update(array('title' => $show->title), $show, array('upsert' => true, 'fsync' => true));

        $returnedItem = (object) $c->findOne();
        return $this->model->get($returnedItem, 'Show');
    }


    public function get () {

        if (false) {
            $show = $this->mongoDB->Show->findOne();
            $show = $this->model->get($show, 'Show');
        } else {
            $this->importTables();
            $show = $this->createShow();

            $results = new stdClass();
            $results->id = rand(0,500);
            $results->user_id = isset($this->user->id) ? (int) $this->user->id : 1;
            $results->show_id = $show->id;
            $results->user = $this->user;

            foreach ($show->matches as $match) {
                $subObject = new stdClass();
                $subObject->id = (int) $match->id;
                $subObject->results = $match->results;
                $results->matches[] = $subObject;
            }
            $this->mongoDB->Show_Prediction->insert((object) $results);

        }

        $results = $this->mongoDB->Show_Prediction->findOne();
        $results = $this->model->get($results, 'Show_Prediction');

        $this->compareResults($show, $results);
        // return array('results' => $results, 'show' => $show, 'wrestlers' => $this->wrestlersArray, 'championships' => $this->championshipArray);
    }


    public function compareResults($showResults, $userResults) {
        /* show result map */
        $showResultsMap = array();
        foreach ($showResults->matches as $match) {
            foreach ($match->results as $key => $result) {
                $showResultsMap[$match->id][$key] = array(
                    'winner' => $result->winner->id,
                    'total-results' => count($match->results),
                    'type' => $match->type,
                    'loser' => $result->loser->id,
                    'by' => $result->by
                );
            }
        }
        /* user result map */
        $userResultsMap = array();
        foreach ($userResults->matches as $match) {
            foreach ($match->results as $key => $result) {
                $userResultsMap[$match->id][$key] = array('winner' => $result->winner->id, 'loser' => $result->loser->id, 'by' => $result->by);
            }
        }

        $totalPoints = 0;
        foreach ($showResultsMap as $key => $result) {
            $points = 0;

            foreach ($result as $subKey => $subResult) {
                if (isset($userResultsMap[$key][$subKey]['winner']) && ($userResultsMap[$key][$subKey]['winner'] == $showResultsMap[$key][$subKey]['winner'])) {
                    $points++;
                    if (isset($userResultsMap[$key][$subKey]['by']) && ($userResultsMap[$key][$subKey]['by'] === $showResultsMap[$key][$subKey]['by'])) {
                        $points++;
                        if (isset($userResultsMap[$key][$subKey]['loser']) && ($userResultsMap[$key][$subKey]['loser'] === $showResultsMap[$key][$subKey]['loser'])) {
                            $points++;
                        }
                    }
                }

                switch($subResult['type']) {
                    default:
                        /* if its a singles match, you dont ever get more than 2 points (winner and how, loser doesnt matter */
                        if ($points > 2) {
                            $points = 2;
                        }
                        break;
                }
                $totalPoints = $totalPoints + $points;
            }
        }
        echo "points: ".$totalPoints;exit;
        print_r($userResultsMap);
        print_r($showResultsMap);exit;

    }

}