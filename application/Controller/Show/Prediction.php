<?php


class Controller_Show_Prediction extends System_Controller {


    public $shows;


    public function init() {
        $this->skipAuthentication();
        $this->id 		        = $this->_getId();
        $this->count 	        = (bool) $this->_getParam('count', false);
        $this->tableName        = 'Show_Prediction';
        $this->tableCollection	= $this->mongoDB->{$this->tableName};
        $this->_getWrestlersIdArray();
    }


    public function post () {

        $this->model->setKey('currentTableName', $this->tableName);
        $this->model->setKey('currentObject', $this->variables);

        try {
            $document       = $this->model->post($this->variables);
            $generatedId    = $this->generateId($this->tableCollection);
            $document->_id  = (string) $generatedId;
            $document->id   = (int) ($generatedId);
            $document->matches = array();

            foreach ($this->variables['matches'] as $match) {
                $results = array();
                $results[] = array(
                    'winner' => $this->wrestlersArray[$match->results->winner->id],
                    'loser' => $this->wrestlersArray[$match->results->loser->id],
                    'by' => $match->results->by
                );
                $document->matches[] = array(
                    'id' => $match->id,
                    'results' => $results
                );
            }

            //$document->user_id = $this->user_id;
            //$document->user = $this->user;

            if ($this->tableCollection->insert($document)) {
                return $document;
            } else {
                return $this->setError(880, array('model' => $document));
            }


        } catch (Exception $e) {
            $this->setError(800, array('thrown' => unserialize($e->getMessage()), 'sent' => $this->variables));

        } catch(MongoCursorException $e) {
            $this->setError(800, serialize(array($e->getMessage())));
        }
    }


    public function get () {

        $this->model->setKey('currentTableName', $this->tableName);
        $this->model->setKey('currentObject', $this->variables);

        $this->getPreDispatch();

        if ($this->count) {
            return array('count' => $this->collection);

        } else if (is_numeric($this->id) && empty($this->collection)) {
            $this->setError(404, array('id' => $this->id));

        } else {
            $this->getPostDispatch();
            return $this->results;
        }
    }


    private function getPreDispatch() {

        /* get the default filters: from, to, order_by, order_direction */
        $this->getDefaultFilters($this->tableName);

        /* get custom filters for this controller */
        $this->getFilters();

        if ($this->count) {
            $this->collection = $this->tableCollection->count($this->filters->items);

        } else if (!is_numeric($this->id)) {
            $this->collection = $this->tableCollection
                ->find($this->filters->items)
                ->limit($this->filters->to)
                ->skip($this->filters->from)
                ->sort(array($this->filters->orderBy => ($this->filters->orderDirection == 'ASC' ? 1 : -1))
                );
        } else {
            $this->collection = $this->tableCollection->findOne(array('id' => (int) $this->id));
        }
    }


    /* create the filters for the GET request */
    private function getFilters() {

        /* preset the default items array so were php 5 strict */
        $this->filters->items = array();

        //if (isset($this->variables['user_id'])) {
        $this->filters->items['user_id'] = (int) 1;// $this->variables['user_id'];
        //}
        if (isset($this->variables['show_id'])) {
            $this->filters->items['show_id'] = (int) $this->variables['show_id'];
        }
    }


    /* process our collection and do any clean up required
     * our mapping system will turn any fields declared into the right type
    */
    private function getPostDispatch() {
        if (!empty($this->collection)) {
            $this->collection = is_numeric($this->id) ? array($this->collection) : $this->collection;
            foreach ($this->collection as $document) {
                $document = $this->model->get($document);
                $document = $this->_score($document);
                $this->results[] = $this->stripExcess($document);
            }
            //temp hack, i only want my predictions
            $this->results = true ? $this->results[0] : $this->results;
        }
    }


    private function _getWrestlersIdArray() {
        $params = array('from' => 0, 'to' => COUNT_HIGH, 'tableName' => 'Wrestler');
        $wrestlers = $this->internalCall(array('controller' => 'Mimic_Index', 'params' => $params));
        foreach ($wrestlers as $wrestler) {
            $this->wrestlersArray[$wrestler->id] = $wrestler;
        }
    }

    private function _score($document) {

        if (!isset($this->shows[$document->show_id])) {
            $this->shows[$document->show_id] = $this->model->get($this->mongoDB->Show->findOne(array('id' => (int) $document->show_id)), 'Show');
        }
        $show = $this->shows[$document->show_id];
        $showPredictions = $this->model->get($document, 'Show_Prediction');
        $results = array();

        /* show result map */
        $showPredictionsMap = array();
        foreach ($show->matches as $key => $match) {
            foreach ($match->results as $key => $result) {
                $showPredictionsMap[$match->id][$key] = array(
                    'teams'             => $match->teams,
                    'total-results'     => count($match->results),
                    'type'              => $match->type,

                    'winner'            => $result->winner,
                    'loser'             => $result->loser,
                    'by'                => $result->by
                );
            }
        }
        /* user result map */
        $userResultsMap = array();
        foreach ($document->matches as $match) {
            foreach ($match->results as $key => $result) {
                $userResultsMap[$match->id][$key] = array('winner' => $result->winner, 'loser' => $result->loser, 'by' => $result->by);
            }
        }

        $totalPoints = 0;
        foreach ($showPredictionsMap as $key => $result) {
            $points = 0;

            foreach ($result as $subKey => $subResult) {
                if (isset($userResultsMap[$key][$subKey]['winner']->id) && ($userResultsMap[$key][$subKey]['winner']->id === $showPredictionsMap[$key][$subKey]['winner']->id)) {
                    $points++;
                    $userResultsMap[$key][$subKey]['result']->winner = array('correct' => true, 'guess' => $userResultsMap[$key][$subKey]['winner']->alias, 'answer' => $showPredictionsMap[$key][$subKey]['winner']->alias);
                    if (isset($userResultsMap[$key][$subKey]['by']) && ($userResultsMap[$key][$subKey]['by'] === $showPredictionsMap[$key][$subKey]['by'])) {
                        $points++;
                        $userResultsMap[$key][$subKey]['result']->by = array('correct' => true, 'guess' => $userResultsMap[$key][$subKey]['by'], 'answer' => $showPredictionsMap[$key][$subKey]['by']);

                        if (isset($userResultsMap[$key][$subKey]['loser']->id) && ($userResultsMap[$key][$subKey]['loser']->id === $showPredictionsMap[$key][$subKey]['loser']->id)) {
                            $points++;
                            $userResultsMap[$key][$subKey]['result']->loser = array('correct' => true, 'guess' => $userResultsMap[$key][$subKey]['loser']->alias, 'answer' => $showPredictionsMap[$key][$subKey]['loser']->alias);
                        } else {
                            $userResultsMap[$key][$subKey]['result']->loser = array('correct' => false, 'guess' => $userResultsMap[$key][$subKey]['loser']->alias, 'answer' => $showPredictionsMap[$key][$subKey]['loser']->alias);
                        }
                    } else {
                        $userResultsMap[$key][$subKey]['result']->by = array('correct' => false, 'guess' => $userResultsMap[$key][$subKey]['by'], 'answer' => $showPredictionsMap[$key][$subKey]['by']);
                    }
                } else {
                    $userResultsMap[$key][$subKey]['result']->winner = array('correct' => false, 'guess' => $userResultsMap[$key][$subKey]['winner']->alias, 'answer' => $showPredictionsMap[$key][$subKey]['winner']->alias);
                }

                $userResultsMap[$key][$subKey]['teams'] = $showPredictionsMap[$key][$subKey]['teams'];

                switch($subResult['type']) {
                    default:
                        /* if its a singles match, you dont ever get more than 2 points (winner and how, loser doesnt matter */
                        if ($points > 2) {
                            $points = 2;
                        }
                        break;
                }
                $totalPoints = $totalPoints + $points;
                $userResultsMap[$key][$subKey]['points'] = array('points' => $points, 'total' => $totalPoints);
            }
        }
        foreach ($userResultsMap as $key => $map) {
            $results['matches'][] = array('id' => $key, 'match' => $map);
        }

        $user = $this->mongoDB->User->findOne(array('id' => (int) $document->user_id));
        $user = $this->model->get($user, 'User');
        $show = $this->model->get($show, 'Show');
        $results = array('points' => array('total' => $totalPoints), 'user' => $user, 'show' => $show, 'matches' => $results['matches']);


        return $results;
    }


}