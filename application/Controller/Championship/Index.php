<?php


class Controller_Championship_Index  extends System_Controller {


    public function init () {

        $this->skipAuthentication();
    }


    public function get() {
        $this->variables['tableName'] = 'Championship';
        return $this->internalCall(array('method' => 'get', 'controller' => 'Mimic_Index','params' => $this->variables));
    }


}