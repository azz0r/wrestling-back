<?php


class Controller_User_Email  extends System_Controller
{


	public function get () {

		if (!isset($this->variables['id']) || !is_numeric($this->variables['id']) || empty($this->variables['id'])) {
			return $this->setError(16, $this->variables);
		}

		if ( $this->variables['id'] != $this->user->id && $this->administrator === false ) {
			return $this->setError(607, array('Requires permission to view this client'));
		}

		$user = (object) $this->mongoDB->User->findOne(array('id' => (int) $this->variables['id']));

		if (empty($user)) {
			return $this->setError(404, $this->variables['id']);
		}

		$action = $this->_getParam('email', 'send-login');

		switch ($action) {
			default:
				return $this->setError(404);
			break;
			case 'send-login':
				$user->password = System_Password::decrpyt($user->encrypted_password);
				new System_Notification(array('controller' => 'user', 'action' => 'added', 'from' => $user->email, 'data' => $user, 'require_row' => false));
			break;
			case 'check-stage':
				$data = array('send_to' => $user, 'sent_from' => $this->user);
				new System_Notification(array('controller' => 'stage', 'action' => 'check', 'from' => $user->email, 'data' => $data, 'require_row' => false));
			break;
		}

		return true;
	}


}