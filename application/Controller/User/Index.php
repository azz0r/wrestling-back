<?php


/* 
DELETE:
curl --digest --user dev+admin@theaudience.com:android 'https://api.devweb.localvm:8443/user?debug=true' -k -X POST --data "id=321&_method=delete"

PUT:
Fail
curl --digest --user dev+admin@theaudience.com:android 'https://api.devweb.localvm:443/user?debug=true' -k -X POST --data "&id=1&first_name=test&last_name=user&email=aaron&internal=0&enabled=0&location_id=0&deleted=0&system=0&_method=put"
Pass
curl --digest --user dev+admin@theaudience.com:android 'https://api.devweb.localvm:443/user?debug=true' -k -X POST --data "&id=1&first_name=test&last_name=user&email=aaron@theaudience.com&internal=0&enabled=0&location_id=0&deleted=0&system=0&_method=put"

POST:
Fail
curl --digest --user dev+admin@theaudience.com:android 'https://api.devweb.localvm:443/user?debug=true' -k -X POST --data "&first_name=test&last_name=user&email=aaron&internal=0&enabled=0&location_id=0&deleted=0&system=0"
Pass
curl --digest --user dev+admin@theaudience.com:android 'https://api.devweb.localvm:443/user?debug=true' -k -X POST --data "&first_name=test&last_name=user&email=aaron@theaudience.com&internal=0&enabled=0&location_id=0&deleted=0&system=0"
*/


class Controller_User_Index extends System_Controller {


    const tableName = 'User';


    public function init() {
        $this->skipAuthentication();
        $this->id 		= $this->_getId();
        $this->count 	= (bool) $this->_getParam('count', false);
        $this->tableCollection	= $this->mongoDB->{self::tableName};

    }


    public function get () {

        if ($this->user->internal === false) {
            return $this->setError(944);
        }

        $this->model->setKey('currentTableName', self::tableName);
        $this->model->setKey('currentObject', $this->variables);

        $this->getPreDispatch();

        if ($this->count) {
            return array('count' => $this->collection);

        } else if (is_numeric($this->id) && empty($this->collection)) {
            $this->setError(404, array('id' => $this->id));

        } else {
            $this->getPostDispatch();
            return $this->results;
        }
    }


    private function getPreDispatch() {

        /* get the default filters: from, to, order_by, order_direction */
        $this->getDefaultFilters(self::tableName);

        /* get custom filters for this controller */
        $this->getFilters();

        if ($this->count) {
            $this->collection = $this->tableCollection->count($this->filters->items);

        } else if (!is_numeric($this->id)) {
            $this->collection = $this->tableCollection
                ->find($this->filters->items)
                ->limit($this->filters->to)
                ->skip($this->filters->from)
                ->sort(array($this->filters->orderBy => ($this->filters->orderDirection == 'ASC' ? 1 : -1))
            );
        } else {
            $this->collection = $this->tableCollection->findOne(array('id' => (int) $this->id));
        }
    }


    /* create the filters for the GET request */
    private function getFilters() {

        /* preset the default items array so were php 5 strict */
        $this->filters->items = array();

        /* request objects that are enabled or disabled */
        if (isset($this->variables['enabled'])) {
            $this->filters->items['enabled'] = $this->model->getCleanValue('enabled', 'fields');
        }

        /* request objects with that have or have not been deleted*/
        if (isset($this->variables['deleted'])) {
            $this->filters->items['deleted'] = $this->model->getCleanValue('deleted', 'fields');
        }

        /* request objects with that have or have not been deleted*/
        if (isset($this->variables['system'])) {
            $this->filters->items['system'] = $this->model->getCleanValue('system', 'fields');
        }

        /* request objects with that have or have not been deleted*/
        if (isset($this->variables['internal'])) {
            $this->filters->items['internal'] = $this->model->getCleanValue('internal', 'fields');
        }

        /* filter by email address */
        if (isset($this->variables['email'])) {
            /* + becomes a space in a URL so we do a replace here so it works */
            $this->filters->items['email'] = (string) str_replace(' ', '+', $this->variables['email']);
        }

        /* 0 = both, 1 = LA, 2 = London: filter objects by the area the user is requestion
         * if the user isnt requestion to filter by location_id or its 0, then we dont apply the filter */
        if (isset($this->variables['location_id']) && $this->variables['location_id'] != 0) {
            $this->filters->items['location_id'] = $this->model->getCleanValue('location_id', 'fields');
        }
    }


    /* process our collection and do any clean up required
     * our mapping system will turn any fields declared into the right type
     */
    private function getPostDispatch() {
        if (!empty($this->collection)) {
            $this->collection = is_numeric($this->id) ? array($this->collection) : $this->collection;
            foreach ($this->collection as $document) {
                $document = $this->model->get($document);
                $this->results[] = $this->stripExcess($document);
            }
            $this->results = is_numeric($this->id) ? $this->results[0] : $this->results;
        }
    }


    /* create a new document */
    public function post() {

        /* only internal users can add a user */
        if ($this->user->internal) {
            $this->model->setKey('currentTableName', self::tableName);
            $this->model->setKey('currentObject', $this->variables);

            try {
                $document = $this->model->post();

                if ($validationErrors = $this->model->validateDocument($document)) {
                    return $this->setError(3100, $validationErrors);
                } else {
                    /* get last row */
                    $generatedId = $this->generateId($this->tableCollection);
                    $document->_id = (string) $generatedId;
                    $document->id = (int) ($generatedId);

                    /* generate the password and get it ready for saving */
                    $password = isset($this->variables['password']) ? $this->variables['password'] : System_Password::generate(8);
                    $document->encrypted_password = System_Password::encrypt($password);

                    if ($this->tableCollection->insert($document)) {
                        /* success - email user */
                        $document->password = $password;
                        new System_Notification(array('controller' => 'user', 'action' => 'added', 'from' => $document->email, 'data' => $document, 'require_row' => false)) ? 'true' : 'false';

                        /* return the parse object */
                        return $this->model->get($document);
                    } else {
                        return $this->setError(880, array('model' => $document));
                    }
                }

            } catch (Exception $e) {
                $this->setError(800, array('thrown' => unserialize($e->getMessage()), 'sent' => $this->variables));

            } catch(MongoCursorException $e) {
                $this->setError(800, serialize(array($e->getMessage())));
            }
        } else {
            return $this->setError(14);
        }
    }


    /* update a document */
    public function put() {

        /* if im an admin or its my own account I can update it */
        if ( ($this->administrator === true) || (is_numeric($this->id) && $this->user->id == $this->id)) {
            try {
                $this->model->setKey('currentTableName', self::tableName);
                $this->model->setKey('currentObject', $this->variables);
                $this->collection = $this->tableCollection->findOne(array('_id' => (string) $this->id));

                /* check the object were trying to collect exists */
                if (empty($this->collection)) {
                    $this->setError(404, array('id' => $this->id));
                } else {
                    $document           = $this->model->put($this->variables, $this->collection);
                    $document->id       = (int) $this->collection['_id'];
                    $document->created  = (int) $this->collection['created'];

                    /* password management - if we are changing the password */
                    if (isset($this->variables['password'])) {
                        $document->encrypted_password = System_Password::encrypt($this->variables['password']);
                    } else {
                        $document->encrypted_password = $this->user->encrypted_password;
                    }

                    /* are there any validation errors? else its fine */
                    if ($validationErrors = $this->model->validateDocument($document)) {
                        return $this->setError(3100, $validationErrors);
                    } else {
                        /* upsert = false - create if it doesn't exist, that shouldnt happen */
                        $this->tableCollection->update(array('_id' => (string) $this->collection['_id']), $document, array('upsert' => false, 'fsync' => true));
                        return $this->internalCall(array('method' => 'get', 'controller' => 'User_Index', 'params' => array('id' => $document->id)));
                    }
                }
            } catch(MongoCursorException $e) {
                $this->setError(800, serialize(array($e->getMessage())));
            }
        } else {
            return $this->setError(14);
        }
    }


    /* Delete an document */
    public function delete() {

        if ($this->administrator === true) {
            try {
                if (is_numeric($this->id)) {
                    return $this->tableCollection->remove(array('id' => (int) $this->id), array('justOne' => true));
                } else {
                    return $this->setError(800, array('id'));
                }

            } catch(MongoCursorException $e) {
                $this->setError(800, serialize(array($e->getMessage())));
            }
        } else {
            return $this->setError(607, array('Requires Admin All Permissions.'));
        }
    }


}