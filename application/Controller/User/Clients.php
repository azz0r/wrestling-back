<?php



class Controller_User_Clients extends System_Controller {


    const tableName = 'Client_Manager';


    public function init() {
        $this->id 		= $this->_getId();
        $this->count 	= (bool) $this->_getParam('count', false);
        $this->tableCollection	= $this->mongoDB->{self::tableName};

    }

    public function get () {

        if (!is_numeric($this->id)) {
            return $this->setError(126);
        }

        $this->collection = $this->tableCollection->find(array('user_id' => (int) $this->id));
        $administrationGroups = array(1,9);//groups that are admins

        if (!empty($this->collection)) {
            foreach ($this->collection as $collection) {
                $collection = (object) $collection;

                if (in_array($collection->group_id, $administrationGroups)) {
                    return array('administrator' => true);
                }

                $result = $this->internalCall(array('controller' => 'Client_Index', 'id' => (int) $collection->client_id));

                if (is_array($result) && isset($result['errors'])) {
                    continue;
                } else {
                    $this->results[] = $result;
                }
            }
        }

        return $this->results;
    }


}