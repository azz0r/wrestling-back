<?php


/* todo - update to the new controller standard

POST conversion
PUT conversion
DELETE conversion

*/


class Controller_User_Stage  extends System_Controller {

    public static $postRequired = array('client_id', 'email', 'first_name', 'last_name', 'groups');

    public static $putRequired = array('client_id', 'id', 'email', 'first_name', 'last_name', 'groups');

	public function get () {

		$clientId = isset($this->variables['client_id']) ? $this->variables['client_id'] : null;
        $originalUserId = $userId = isset($this->variables['user_id']) ? $this->variables['user_id'] : null;//we bind to originalUserId to return it as one item, not an array
		
		//required client id
		if (!is_numeric($clientId)) {
			return $this->setError(404);
		}

		$this->getClientIds(array('controller' => 'client', 'action' => 'index'));
		
		if (!in_array($clientId, $this->clientIds) && $this->administrator === false) {
			return $this->setError(454);
		} else {
            /* get the groups and turn them into a keyed array */
		    $groupQuery = $this->internalCall(array('controller' => 'Group_Index'));

			foreach ($groupQuery as $group) {
				$group = (object) $group;
				$groups[$group->id] =  $this->model->get($group, 'Group');
			}
			/* end get the groups */

            /* get the client manager rows */
			$clientManagerFilters = array();
			$clientManagerFilters['client_id'] = (int) $clientId;
			if (is_numeric($userId)) {
				$clientManagerFilters['user_id'] = (int) $userId;
			}

			$clientManagerQuery = $this->mongoDB->Client_Manager->find($clientManagerFilters);
            $clientManagers = array();
			$userIds = array();
			if (!empty($clientManagerQuery)) {
				$this->model->setKey('currentTableName', 'Client_Manager');
				foreach ($clientManagerQuery as $clientManager) {
					$clientManager = $this->model->get($clientManager, 'Client_Manager');
					$clientManagers[] = $clientManager;
					$userIds[] = $clientManager->user_id;
				}
			}


			/*fetch the users*/
			$this->model->setKey('currentTableName', 'Group');
			$userQuery = $this->mongoDB->User->find(array('id' => array('$in' => $userIds)));
			$users = array();
			if (!empty($userQuery)) {
				foreach ($userQuery as $user) {
					$user = $this->model->get($user, 'User');
					$users[$user->id] = $user;
				}
			}
			/* end fetch the users */


			/* now lets create our result */
			$result = array();
			foreach ($users as $user) {
				$user->groups = array();
                if (!empty($clientManagers)) {
                    foreach ($clientManagers as $clientManager) {
                        if ($clientManager->user_id == $user->id) {
                            $user->groups[] = $groups[$clientManager->group_id];
                        }
                    }
                }
			}
			
			$users = array_values($users);

		    if (is_numeric($originalUserId)) {
                $users = isset($users[0]) ? $users[0] : $users;
            }

            return $users;
		}
	}


	public function post () {

        // if a required filed is missing or empty lets make it null
		foreach (self::$postRequired as $key) {
			if (!isset($this->variables[$key]) || is_null($this->variables[$key]) || empty($this->variables[$key])) {
				return $this->setError(600, array('fields' => $key, 'sent' => $this->variables));
			}
		}

		$this->getClientIds(array('controller' => 'client', 'action' => 'index'));

		if (!in_array($this->variables['client_id'], $this->clientIds) && $this->administrator === false) {
			return $this->setError(454);
		}

		$user = (object) $this->mongoDB->User->findOne(array('email' => (string) $this->variables['email']));

        if (empty($user) || !isset($user->id)) {
            $insert = array();
            $insert['first_name'] = $this->variables['first_name'];
            $insert['last_name']  = $this->variables['last_name'];
            $insert['email']      = $this->variables['email'];
            $insert['internal']   = false;
            $insert['password']   = System_Password::generate(8);
            $insert['enabled']    = true;
            $user = (object) $this->internalCall(array('controller' => 'User_Index', 'params' => $insert, 'method' => 'post'));
            if (empty($user) || !isset($user->id)) {
                return $this->setError(404);
            }
        }

        return $this->_manageGroups($user, $this->variables['client_id'], $this->variables['groups']);
	}


	public function put () {

        foreach (self::$putRequired as $key) {
			if (!isset($this->variables[$key]) || is_null($this->variables[$key]) || empty($this->variables[$key])) {
				return $this->setError(600, array('fields' => $key, 'sent' => $this->variables));
			}
		}

		$this->getClientIds(array('controller' => 'client', 'action' => 'index'));

		if (!in_array($this->variables['client_id'], $this->clientIds) && $this->administrator === false) {
			return $this->setError(454);
		}

        $user = (object) $this->mongoDB->User->findOne(array('id' => (int) $this->variables['id']));

        if (is_null($user)) {
            return $this->setError(404, array('no user could be loaded with that user id'));
        }

        $toUpdate = array();
        $toUpdate['first_name'] = isset($this->variables['first_name']) && !empty($this->variables['first_name']) ? $this->variables['first_name'] : $user->first_name;
        $toUpdate['last_name'] = isset($this->variables['last_name']) && !empty($this->variables['last_name']) ? $this->variables['last_name'] : $user->first_name;
        $toUpdate['email'] = isset($this->variables['email']) && !empty($this->variables['email']) ? $this->variables['email'] : $user->first_name;
        $toUpdate['internal'] = false; //risky but its essential

        $this->mongoDB->User->update(array('id' => $user->id), array('$set' => $toUpdate));

		return $this->_manageGroups($user, $this->variables['client_id'], $this->variables['groups']);
	}


	public function _manageGroups ($user, $clientId, $groups) {

        $this->mongoDB->Client_Manager->remove(array('client_id' => (int) $clientId, 'user_id' => (int) $user->id));

        if (!empty($groups)) {
			foreach ($groups as $groupId) {
                $id = $this->generateId($this->mongoDB->Client_Manager);
                $group = array(
                    '_id' => $id,
                    'id' => (int) $id,
                    'user_id' => $user->id,
                    'client_id' => (int) $clientId,
                    'group_id' => (int) $groupId,
                    'enabled' => (bool) true,
                    "updated" => 0,
                    "created" => time()
                );
                $group = $this->model->get($group, 'Client_Manager');
                $this->mongoDB->Client_Manager->insert($group);
				$user->groups[] = $group;

			}
		}

        $params = array('client_id' => $clientId, 'user_id' => $user->id);
        return $this->internalCall(array('controller' => 'User_Stage', 'method' => 'get', 'params' => $params));
	}


}