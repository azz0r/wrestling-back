<?php



/* todo - update to the new controller standard

GET conversion

*/


function sort_by ($a, $b) {

	if ($a->created == $b->created) {
		return 0;
	}
	return ($a->created < $b->created) ? +1 : -1;
}


class Controller_User_Notification  extends System_Controller
{


	public function get () {

		$return = array();
		$from = $this->_getParam('timestamp');

        $posts = $this->internalCall(array('method' => 'get', 'controller' => 'Post_Index'));
		
		foreach ($posts as $post) {
			$post->filter = 'Post';
		}

        $images = $this->internalCall(array('method' => 'get', 'controller' => 'Asset_Image'));
		
		foreach ($images as $image) {
			$image->filter = 'Image';
		}
		
		$clients = $this->internalCall(array('method' => 'get', 'controller' => 'Client_Index'));

        foreach ($clients as $client) {
			$client->filter = 'Client';
		}
		
		$this->results = array_merge($posts, $images, $clients);
		
		usort($this->results, 'sort_by');
		
		return $this->results;
	}


}