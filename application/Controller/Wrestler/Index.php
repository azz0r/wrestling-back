<?php
/**
 * User: aaron lote
 * Date: 14/01/2013
 * Time: 13:53
 */


class Controller_Wrestler_Index  extends System_Controller
{


    public function init () {
        $this->skipAuthentication();
    }

    public function get() {
        $this->variables['tableName'] = 'Wrestler';
        $this->variables['to'] = '100';
        return $this->internalCall(array('method' => 'get', 'controller' => 'Mimic_Index','params' => $this->variables));
    }

    public function importAction(){
        $import = array();
        $import['Wrestler'][] = array('alias' => 'Alberto Del Rio');
        $import['Wrestler'][] = array('alias' => 'Wade Barrett');
        $import['Wrestler'][] = array('alias' => 'Antonio Cesaro');
        $import['Wrestler'][] = array('alias' => 'The Rock');
        $import['Wrestler'][] = array('alias' => 'Daniel Bryan');
        $import['Wrestler'][] = array('alias' => 'Kane');
        $import['Wrestler'][] = array('alias' => 'Big Show');
        $import['Wrestler'][] = array('alias' => 'Booker T');
        $import['Wrestler'][] = array('alias' => 'Brock Lesnar');
        $import['Wrestler'][] = array('alias' => 'Brodus Clay');
        $import['Wrestler'][] = array('alias' => 'Chris Jericho');
        $import['Wrestler'][] = array('alias' => 'Christian');
        $import['Wrestler'][] = array('alias' => 'CM Punk');
        $import['Wrestler'][] = array('alias' => 'Cody Rhodes');
        $import['Wrestler'][] = array('alias' => 'Damien Sandow');
        $import['Wrestler'][] = array('alias' => 'Darren Young');
        $import['Wrestler'][] = array('alias' => 'David Otunga');
        $import['Wrestler'][] = array('alias' => 'Dean Ambrose');
        $import['Wrestler'][] = array('alias' => 'Dolph Ziggler');
        $import['Wrestler'][] = array('alias' => 'Great Khali');
        $import['Wrestler'][] = array('alias' => 'Heath Slater');
        $import['Wrestler'][] = array('alias' => 'Jack Swagger');
        $import['Wrestler'][] = array('alias' => 'John Cena');
        $import['Wrestler'][] = array('alias' => 'Justin Gabriel');
        $import['Wrestler'][] = array('alias' => 'Kofi Kingston');
        $import['Wrestler'][] = array('alias' => 'Mark Henry');
        $import['Wrestler'][] = array('alias' => 'The Miz');
        $import['Wrestler'][] = array('alias' => 'Mr. McMahon');
        $import['Wrestler'][] = array('alias' => 'Paul Heyman');
        $import['Wrestler'][] = array('alias' => 'R-Truth');
        $import['Wrestler'][] = array('alias' => 'Randy Orton');
        $import['Wrestler'][] = array('alias' => 'Rey Mysterio');
        $import['Wrestler'][] = array('alias' => 'Roman Reigns');
        $import['Wrestler'][] = array('alias' => 'Ryback');
        $import['Wrestler'][] = array('alias' => 'Santino Marella');
        $import['Wrestler'][] = array('alias' => 'Shaemus');
        $import['Wrestler'][] = array('alias' => 'Seth Rollins');
        $import['Wrestler'][] = array('alias' => 'Sin Cara');
        $import['Wrestler'][] = array('alias' => 'Ted Dibiase');
        $import['Wrestler'][] = array('alias' => 'Tensai');
        $import['Wrestler'][] = array('alias' => "Titus O Niel");
        $import['Wrestler'][] = array('alias' => 'Triple H');
        $import['Wrestler'][] = array('alias' => 'Tyson Kidd');
        $import['Wrestler'][] = array('alias' => 'The Undertaker');
        $import['Wrestler'][] = array('alias' => 'William Regal');
        $import['Wrestler'][] = array('alias' => 'Zack Ryder');

        $import['Wrestler'][] = array('alias' => 'Tamina Snuka', 'male' => false);
        $import['Wrestler'][] = array('alias' => 'Rosa Mendes', 'male' => false);
        $import['Wrestler'][] = array('alias' => 'Vicki Guerrero', 'male' => false);
        $import['Wrestler'][] = array('alias' => 'AJ Lee', 'male' => false);
        $import['Wrestler'][] = array('alias' => 'Aksana', 'male' => false);
        $import['Wrestler'][] = array('alias' => 'Alicia Fox', 'male' => false);
        $import['Wrestler'][] = array('alias' => 'Cameron', 'male' => false);
        $import['Wrestler'][] = array('alias' => 'Kaitlyn', 'male' => false);
        $import['Wrestler'][] = array('alias' => 'Layla', 'male' => false);
        $import['Wrestler'][] = array('alias' => 'Naomi', 'male' => false);
        $import['Wrestler'][] = array('alias' => 'Natalya Hart', 'male' => false);

        foreach ($import as $tableName => $values) {
            $tempDB = $this->mongoDB->{$tableName};
            $tempDB->drop();

            foreach ($values as $subValues) {
                $this->internalCall(array('controller' => 'Mimic_Index', 'method' => 'post',
                    'params' => array('tableName' => $tableName, 'data' => $subValues)));
            }
        }

        $params = array('from' => 0, 'to' => COUNT_HIGH, 'tableName' => 'Wrestler');
        $wrestlers = $this->internalCall(array('controller' => 'Mimic_Index', 'params' => $params));

        $championshipArray = array();
        $this->wrestlersArray = array();
        $championships = $this->internalCall(array('controller' => 'Championship_Index', 'params' => array('from' => 0, 'to' => 100)));

        foreach ($championships as $championship) {
            $this->championshipArray[$championship->id] = $championship;
        }

        $this->wrestlersArray = array();
        foreach ($wrestlers as $wrestler) {
            switch ($wrestler->alias) {
                case 'Alberto Del Rio':
                    $wrestler->championship = $this->championshipArray[2];
                    break;
                case 'Wade Barrett':
                    $wrestler->championship = $this->championshipArray[4];
                    break;
                case 'The Rock':
                    $wrestler->championship = $this->championshipArray[3];
                    break;
                case 'Kane':case 'Daniel Bryan':
                $wrestler->championship = $this->championshipArray[1];
                break;
                case 'Kaitlyn':
                    $wrestler->championship = $this->championshipArray[6];
                    break;
                case 'Antonio Cesaro':
                    $wrestler->championship = $this->championshipArray[5];
                    break;
            }
            if ($wrestler->championship != false) {
                $this->internalCall(array('controller' => 'Mimic_Index', 'method' => 'put','params' => array('tableName' => 'Wrestler', 'data' => $wrestler)));
            }
            $this->wrestlersArray[$wrestler->alias] = $wrestler;
            return $this->wrestlersArray;
        }
    }


}