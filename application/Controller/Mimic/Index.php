<?php


class Controller_Mimic_Index extends System_Controller {



    public function init() {
        $this->skipAuthentication();
        $this->id 		        = $this->_getId();
        $this->count 	        = (bool) $this->_getParam('count', false);
        $this->tableName        = $this->_getParam('tableName');
        $this->tableCollection	= $this->mongoDB->{$this->tableName};

    }


    public function get () {

        $this->model->setKey('currentTableName', $this->tableName);
        $this->model->setKey('currentObject', $this->variables);

        $this->getPreDispatch();

        if ($this->count) {
            return array('count' => $this->collection);

        } else if (is_numeric($this->id) && empty($this->collection)) {
            $this->setError(404, array('id' => $this->id));

        } else {
            $this->getPostDispatch();
            return $this->results;
        }
    }


    private function getPreDispatch() {

        /* get the default filters: from, to, order_by, order_direction */
        $this->getDefaultFilters($this->tableName);

        /* get custom filters for this controller */
        $this->getFilters();

        if ($this->count) {
            $this->collection = $this->tableCollection->count($this->filters->items);

        } else if (!is_numeric($this->id)) {
            $this->collection = $this->tableCollection
                ->find($this->filters->items)
                ->limit($this->filters->to)
                ->skip($this->filters->from)
                ->sort(array($this->filters->orderBy => ($this->filters->orderDirection == 'ASC' ? 1 : -1))
            );
        } else {
            $this->collection = $this->tableCollection->findOne(array('id' => (int) $this->id));
        }
    }


    /* create the filters for the GET request */
    private function getFilters() {

        /* preset the default items array so were php 5 strict */
        $this->filters->items = array();
        $ignore = array('module','action','return','request', 'session_id','http_accept','from','to','order_by','order_direction', 'tableName');
        foreach ($this->variables as $key => $value) {
            if (!in_array($key, $ignore)) {
                $this->filters->items[$key] = $value;
            }
        }
    }


    /* process our collection and do any clean up required
     * our mapping system will turn any fields declared into the right type
     */
    private function getPostDispatch() {
        if (!empty($this->collection)) {
            $this->collection = is_numeric($this->id) ? array($this->collection) : $this->collection;
            foreach ($this->collection as $document) {
                $document = $this->model->get($document);
                $this->results[] = $this->stripExcess($document);
            }
            $this->results = is_numeric($this->id) ? $this->results[0] : $this->results;
        }
    }


    /* create a new document */
    public function post() {

        $this->model->setKey('currentTableName', $this->variables['tableName']);
        $this->model->setKey('currentObject', $this->variables['data']);

        try {
            $document = $this->model->post();

            if ($validationErrors = $this->model->validateDocument($document)) {
                return $this->setError(3100, $validationErrors);
            } else {
                /* get last row */
                $generatedId = $this->generateId($this->tableCollection);
                $document->_id = (string) $generatedId;
                $document->id = (int) ($generatedId);

                if ($this->tableCollection->insert($document)) {
                    return $document;
                } else {
                    return $this->setError(880, array('model' => $document));
                }
            }

        } catch (Exception $e) {
            $this->setError(800, array('thrown' => unserialize($e->getMessage()), 'sent' => $this->variables));

        } catch(MongoCursorException $e) {
            $this->setError(800, serialize(array($e->getMessage())));
        }

    }


    /* update a document */
    public function put() {

        try {
            $this->model->setKey('currentTableName', $this->variables['tableName']);
            $this->model->setKey('currentObject', $this->variables['data']);
            $this->collection = $this->tableCollection->findOne(array('id' => (int) $this->variables['data']->id));

            /* check the object were trying to collect exists */
            if (empty($this->collection)) {
                echo 'empty collection';exit;
                $this->setError(404, array('id' => $this->id));
            } else {
                $document = $this->model->put($this->variables['data'], $this->collection, $this->variables['tableName']);
                $document->id = (int) $this->collection['_id'];

                if ($validationErrors = $this->model->validateDocument($document)) {
                    return $this->setError(3100, $validationErrors);
                } else {
                    $this->tableCollection->update(array('id' => (int) $this->variables['data']->id), $document, array('upsert' => false, 'fsync' => true));
                    return $document;
                }
            }
        } catch(MongoCursorException $e) {
            $this->setError(800, serialize(array($e->getMessage())));
        }
    }



    /* Delete an document */
    public function delete() {

        try {
            if (is_numeric($this->id)) {
                return $this->tableCollection->remove(array('id' => (int) $this->id), array('justOne' => true));
            } else {
                return $this->setError(800, array('id'));
            }

            return true;

        } catch(MongoCursorException $e) {
            $this->setError(800, serialize(array($e->getMessage())));
        }
    }



}