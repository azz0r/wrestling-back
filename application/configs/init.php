<?php 


/*remove slashes from strings!*/
if (get_magic_quotes_gpc()) {
	$strip_slashes_deep = function ($value) use (&$strip_slashes_deep) {
		return is_array($value) ? array_map($strip_slashes_deep, $value) : stripslashes($value);
	};
	$_GET = array_map($strip_slashes_deep, $_GET);
	$_POST = array_map($strip_slashes_deep, $_POST);
	$_COOKIE = array_map($strip_slashes_deep, $_COOKIE);
}


define('_LIBRARY_PATH_', (getenv('_LIBRARY_PATH_') ? getenv('_LIBRARY_PATH_') : dirname(__FILE__)) . '/library');


set_include_path(implode(PATH_SEPARATOR, array(
    BASE_DIR . '/library',
    _LIBRARY_PATH_.'/',
    get_include_path(),
)));



//check which class were going to load
function __autoload($class_name) {
	
	$class_name = str_replace ( '_', '/', $class_name );
	
	if (file_exists ( _CLASS . DS . $class_name . '.php' )) {
		require (_CLASS . DS . $class_name . '.php');
	} else if (file_exists ( _CLASS_APP . DS . $class_name . '.php' )) {
		require (_CLASS_APP . DS . $class_name . '.php');
	} else if (file_exists(_LIBRARY_PATH_.DS.$class_name.'.php')) {
		require (_LIBRARY_PATH_.DS.$class_name.'.php');
	} else {
		echo '<pre>'.var_dump(debug_backtrace()).'</pre>';
		echo $class_name . ' Could not be loaded<br />';
		exit ();
	}
}


function decrypt_password($encpassword) {
  $key = '14a4e4e1919096900b70279f3da924b243358073';
  $password = @openssl_decrypt($encpassword, 'BF-ECB', $key);
  return $password;
}


function http_digest_parse($txt) {

    $needed_parts = array('nonce'=>1, 'nc'=>1, 'cnonce'=>1, 'qop'=>1, 'username'=>1, 'uri'=>1, 'response'=>1);
    $data = array();
    $keys = implode('|', array_keys($needed_parts));

    preg_match_all('@(' . $keys . ')=(?:([\'"])([^\2]+?)\2|([^\s,]+))@', $txt, $matches, PREG_SET_ORDER);

    foreach ($matches as $m) {
        $data[$m[1]] = $m[3] ? $m[3] : $m[4];
        unset($needed_parts[$m[1]]);
    }

    return $needed_parts ? false : $data;
}



function datetime() {
	return date('Y-m-d H:i:s', time());
}


function get_value($id, $default_to=NULL) {

	if (isset($_GET[$id])) {
		return $_GET[$id];
	} else {
		return $default_to;
	}
}


function d($d){
	print '<pre>';
	print_r($d);
	print '</pre>';
}


function dv($d){
	print '<pre>';
	var_dump($d);
	print '</pre>';
}


//clean inputs !
if (get_magic_quotes_gpc()) {
	$_GET 		= array_map('stripslashes', $_GET);
	$_POST 		= array_map('stripslashes', $_POST);
	$_COOKIE 	= array_map('stripslashes', $_COOKIE);
}