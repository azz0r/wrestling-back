<?php


/*sets common defined vars*/
new System_Define();



/* @todo this should come from the config */
error_reporting(E_ALL);
ini_set("display_errors", 'On');
/* end @todo this should come from the config */


define('VERSION', 					$this->config->version);
define('SITE_URL',					$this->config->site_url);


/*image link us used as the default link to an image if its not uploaded to the cloud*/
define('IMAGE_LINK',				$this->config->image_link);
/*used for the images server path*/
define('IMAGE_DIR',					$this->config->image_dir);


/*api sends out alerts, we need these settings*/
define('EMAIL_LINK',				$this->config->email_link);
/*this one should be defined by the user, it allows them to receieve the email to an address they want*/
define('EMAIL_TEST',				$this->config->email_test);


/* Folder Routes */
define('_CACHE',					$this->config->_cache); # CACHE folder
define('_PUBLIC', 					SITE_URL.'public'); # PUBLIC folder
define('_LOG',						$this->config->_log); # LOG directory, uses Zend_Log


/* Logging priority
    Zend_Log::EMERG: Emergency: system is unusable
    Zend_Log::ALERT: Alert: action must be taken immediately
    Zend_Log::CRIT: Critical: critical conditions
    Zend_Log::ERR: Error: error conditions
    Zend_Log::WARN: Warning: warning conditions
    Zend_Log::NOTICE: Notice: normal but significant condition
    Zend_Log::INFO: Informational: informational messages
    Zend_Log::DEBUG: Debug: debug messages
 */
define('LOG_PRIORITY', Zend_Log::ERR);


/*assets are backup up to the cloud, lets define those settings*/
define('ASSET_CLOUD', 						$this->config->asset_cloud); //if true, were backing up the cloud
define('ASSET_CLOUD_PUBLIC_URL', 			$this->config->asset_cloud_public_url);
define('ASSET_CLOUD_UPLOAD_URL', 			$this->config->asset_cloud_upload_url);
define('ASSET_CLOUD_UPLOAD_ACCESS_KEY', 	$this->config->asset_cloud_upload_access_key);
/*upload timeout, in seconds*/
define('ASSET_CLOUD_UPLOAD_TIMEOUT',        isset($this->config->asset_cloud_upload_timeout) ? $this->config->asset_cloud_upload_timeout : 3);


/* EXTERNAL API KEYS */
    /* flickr */
define('SDK_FLICKR_API_KEY', isset($this->config->sdk_flickr_api_key) ? $this->config->sdk_flickr_api_key : 1);
define('SDK_FLICKR_API_SEC', isset($this->config->sdk_flickr_api_sec) ? $this->config->sdk_flickr_api_sec : 1);
    /* guardian */
define('SDK_GUARDIAN_API_KEY', isset($this->config->sdk_guardian_api_key) ? $this->config->sdk_guardian_api_key : 1);


/*hydra master*/
define('ANALYTICS_URL', 			$this->config->analytics_url);
define('ANALYTICS_ACCESS_TOKEN', 	$this->config->analytics_access_token);
define('ANALYTICS_USERNAME',		$this->config->analytics_username);
define('ANALYTICS_PASSWORD',		$this->config->analytics_password);


/*is ssl enabled?*/
define('SSL',						true);
define('SSL_CERTIFICATION_PATH',	'/etc/ssl/certs');
define('SSL_VERIFICATION',			false);



/* googleplus publishing */
define('GOOGLE_PUBLISH_CLIENT_ID',
    isset($this->config->googleplus_publish_client_id) ? $this->config->googleplus_publish_client_id : false);
define('GOOGLE_PUBLISH_CLIENT_SECRET',
    isset($this->config->googleplus_publish_client_secret) ? $this->config->googleplus_publish_client_secret : false);
define('GOOGLE_PUBLISH_CLIENT_URL',
    isset($this->config->googleplus_publish_client_url) ? $this->config->googleplus_publish_client_url : false);
define('GOOGLE_PUBLISH_DEVELOPER_KEY',
    isset($this->config->googleplus_publish_developer_key) ? $this->config->googleplus_publish_developer_key : false);



/*set PHP environment variables*/
set_time_limit($this->config->set_time_limit);


//session - so we need session_path, session_time and session_handler from the ini*/
session_cache_expire($this->config->session_time);
session_save_path($this->config->session_path);


ini_set('session.cookie_lifetime',      $this->config->session_time);
ini_set('session.save_handler',         isset($this->config->session_handler) ? $this->config->session_handler : 'files');


/* MONGO CONFIGURATION */
define('MONGO', false);

/* TWITTER API */
define('CURRENT_TWITTER_API_VERSION', '1.1');
define('TWITTER_API_URL', 'https://api.twitter.com/'.CURRENT_TWITTER_API_VERSION.'/');