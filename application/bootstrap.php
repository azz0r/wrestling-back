<?php 



/**
 * Main Bootstrap class 
 *
 */
class Bootstrap {


	protected $rest;
	protected $db;
	protected $object;
	public $config;
	const CONFIG_FILE_PATH = INI_PATH;


	/**
	 * Includes autoloader and some constants
	 * Loads config files as well
	 * @todo change autoloader to object
	 * @todo clear crap from env file
	 */
	public function __construct() {	
		$config = new Zend_Config_Ini(self::CONFIG_FILE_PATH);
		Zend_Registry::set('config', $config);	
		$this->config = $config;
		require_once(_CONFIG . DS . 'define.php');

		defined('_MODULE_') || define('_MODULE_', (isset($_GET['module'])  ? $_GET['module'] : _MODULE_DEFAULT));
		defined('_ACTION_') || define('_ACTION_', (isset($_GET['action'])  ? $_GET['action'] : _ACTION_DEFAULT));

		return $this;
	}


	/**
	 * Initializes Rest. Generally if that fails we don't want do to anything else then just echo the problem and exit
	 */
	public function initRest() {

		try {
			$this->rest = System_Rest::processRequest();

		} catch (Exception $e) {
			echo $e->getMessgae();
			exit;
		}
		return $this;
	}

//
//	public function initDb() {
//
//		$db	= new System_Database("mysql:host={$this->config->db_host};dbname={$this->config->db_table}", $this->config->db_user, $this->config->db_pass);
//		$this->db = $db;
//
//		$zenddb = new Zend_Db_Adapter_Pdo_Mysql(array(
//		    'host'     => $this->config->db_host,
//		    'username' => $this->config->db_user,
//		    'password' => $this->config->db_pass,
//		    'dbname'   => $this->config->db_table
//		));
//		Zend_Registry::set('db', $zenddb);
//		Zend_Db_Table::setDefaultAdapter($zenddb);
//
//		return $this;
//	}

    public function initDb() {
        $auth = Zend_Auth::getInstance();
        $mongoDb = new Mongo(); // @todo fix getting values from DB
        $this->db = $mongoDb->website;
        $collection = $this->db->User;
        $authAdapter = new Zend_Auth_Adapter_MongoDb($collection, 'id', 'encrypted_password');
        Zend_Registry::set('authAdapter', $authAdapter);

        return $this;
    }

    public function initSession() {
        $sessionManager = new System_Authentication_Session_Handler();
        Zend_Session::setOptions(array(
            'gc_probability' => 1,
            'gc_divisor' => 5000,
            'use_cookies' => 'off',
            'use_only_cookies' => 'off',
        ));
        Zend_Session::setSaveHandler($sessionManager);
    }


	public function getRest() {
		return $this->rest;
	}	


	public function run() {
        // Now set session save handler to our custom class which saves the data in MongoDB database
        new System_Authentication_Session();
		System_Authentication_Init::getInstance($this->rest);
		$this->_prepare();

		$start_time 	= microtime(true);
		$last_modified 	= filemtime(dirname(__FILE__));
		$max_age 		= 17280000;//200 days

		header('Last-Modified: '.date('r', $last_modified));
		header('Expires: '.date('r', $last_modified + $max_age));
		header('ETag: '.dechex($last_modified));
		header("Cache-Control: must-revalidate, proxy-revalidate, max-age=$max_age, s-maxage=$max_age");
		
		$this->object->execute();
		return $this->object->send();
	}


	protected function _prepare() {

		$logger = Zend_Registry::get('logger');
		$objectName = $this->generateObjectName();
		
		$file = ucwords(str_replace('-', '',$this->reCase(_MODULE_)));
		$file = 'Controller_'.$file.'_'.$this->reCase(_ACTION_);
		$file = ucwords(str_replace('_', '/', $file)).'.php';

        if (!file_exists(_APP.'/'.$file)) {
			throw new System_Exception_Application(1);
		}

		$logger->debug("AUTO: {$file} ".strtoupper($this->rest->getMethod()));

		$object = new $objectName($this->rest);
		$this->object = $object;

		if (!is_object($object)) {
			throw new System_Exception_Application(666);
		}
	}
	
	//this function changes case from word_word to Word_Word
	public function reCase($string){
		if (isset($string) && !empty($string)){
			$words = explode('_', $string);
			foreach($words as $word){
				$newWords[] = ucfirst(strtolower($word));
			}	
			return implode('_', $newWords);
		}
	}


	/**
	 * Generates object name for api call 
	 * @return $object_temp string 
	 */
	public function generateObjectName() {
		$module = ucwords(str_replace('-', '',$this->reCase(_MODULE_)));
		$object_temp = 'Controller_'.$module.'_'.$this->reCase(_ACTION_);
		return $object_temp;
	}


	public function initLog() {
		new System_Log();
		return $this;
	}


	/**
	 * Processes exception. 
	 * @param Exception $e
	 */
	public function proccessException(Exception $e) {
		if (is_object($e)) {
			$error = $e->getMessage();
		} else {
			$error = $e;
		}
		$this->rest->setErrorData($error);
		$this->rest->sendResponse();
	}


}