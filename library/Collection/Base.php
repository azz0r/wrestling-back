<?php


class Collection_Base {

    public static function get_db_connection() {
        return System_Database_Mongo::getConnection();
    }

    /* function to generate a new _id in a MongoDB Table */
    protected function generateId($tableCollection) {
        /* this is a temporary solution - @todo
         * we retrieve the last row of the collection passed and return it +1
         * this is because 32php makes a large int -2342423 if its over the threshold.
         */
        $lastRow = $tableCollection->find()->sort(array('id' => -1))->limit(1);
        foreach ($lastRow as $row) {
            return (int) $row['id'] + 1;
        }
        /* we cant do this with 32 bit PHP, switch back to this when we have it */
        #return $this->user->id.time();
        throw new Exception ("We couldn't generate a unique id");
    }

    public static function appendAutomaticFieldsForCollection($documents) {

        $parentClass = get_called_class();

        if (!empty($documents)) {
            foreach ($documents as $key => $document) {
                $documents[$key] = $parentClass::appendAutomaticFields($document);
            }
        }
        return $documents;
    }

    public static function create($fields) {

        $db = Collection_Base::get_db_connection();
        $generateId = isset($fields['generate_id']) ? $fields['generate_id'] : false;

        $tableName = isset($fields['table_name']) ? $fields['table_name'] : NULL;
        if ($tableName == NULL) {
            throw new exception('You must specify a table.');
        }
        unset($fields['table_name']);

        if($generateId) {
            $fields['_id'] = (string) Collection_Base::generateId($db->{$tableName});
            $fields['id'] = (int) $fields['_id'];
            unset($fields['generate_id']);
        }

        $fields['created'] = isset($fields['created']) ? $fields['created'] : time();

        $return = $db->{$tableName}->insert($fields);
        return !is_null($return['err']) ? false : $return['ok'];
    }

    /* accepts: from, to, count, table_name, find_one, no_baseclass
        returns: filtered result based on input sent in fields
    */
    public static function find($fields=array()) {

        $db             = Collection_Base::get_db_connection();
        $criteria       = array();
        $findOne        = false;
        $noBaseclass    = isset($fields['no_baseclass']) ? (bool) $fields['no_baseclass'] : false;
        $findOne        = isset($fields['find_one']) ? (bool) $fields['find_one'] : true;
        $count          = isset($fields['count']) ? $fields['count'] : false;
        $from           = isset($fields['from']) ? $fields['from'] : 0;
        $to             = isset($fields['to']) ? $fields['to'] : 50;

        if (isset($fields['table_name'])) {
            $tableName = $fields['table_name'];
        } else {
            throw new exception('You must specify a table.');
        }

        /* unset any fields that might not be relevant to the query */
        $unsets = array('table_name', 'no_baseclass', 'find_one', 'no_tablename', 'from', 'to', 'count');
        foreach ($unsets as $unset) {
            if(isset($fields[$unset])) unset($fields[$unset]);
        }

        /* create our criteria */
        foreach ($fields as $key => $value) {
            if ($value == 'NULL' || is_null($value)) {
            } else {
                $criteria[$key] = $value;
            }
        }

        if($findOne) {
            $item = (object) $db->{$tableName}->findOne($criteria);
            return ($noBaseclass === true) ? $item : new self($item);
        } else {
            if ($count == true) {
                return (int) $db->{$tableName}->count($criteria);
            } else {
                $collection =
                    $db->{$tableName}
                        ->find($criteria)
                        ->limit($to)
                        ->skip($from);
                $results = array();
                if (!empty($collection)) {
                    foreach ($collection as $document) {
                        $results[] = ($noBaseclass === true) ? $document : new self($document);
                    }
                }
                return $results;
            }
        }
    }

    public static function update($fields, $criteria) {
        $db = Collection_Base::get_db_connection();

        $tableName = isset($fields['table_name']) ? $fields['table_name'] : NULL;
        if ($tableName == NULL) {
            throw new exception('You must specify a table.');
        }

        unset($fields['table_name']);
        $db->{$tableName}->update($criteria, array('$set' => $fields));
    }

    public static function delete($fields = array(), $criteria = array()) {

        $tableName = isset($fields['table_name']) ? $fields['table_name'] : NULL;
        if (is_null($tableName)) {
            throw new exception('You must specify the name of the table');
        }

        $tableName = $fields['table_name'];

        $db = Collection_Base::get_db_connection();
        return $db->{$tableName}->remove($criteria);
    }

    public static function query($filters=array(), $criteria=array(), $showAll = 0) {
        $db = Collection_Base::get_db_connection();
        // default filters
        $from = 0;
        $to = 20;
        $order = array('_id' => 1);
        $count = false;
        $findOne = false;
        $noTablename = true;
        $noBaseclass = true;

        if($showAll) {
            $from = null;
            $to = null;
        }

        $tableName = isset($filters['table_name']) ? $filters['table_name'] : null;

        if(is_null($tableName)) throw new exception('You must specify a table.');
        unset($filters['table_name']);
        $arrayFilters = array();

        foreach($filters as $key => $filter) {
            ${$key} = $filter;
        }

        $item = new stdClass();
        if($count) {
            $item = $db->{$tableName}->count($criteria);
            return $item;
        } else {
            if($findOne) {
                $item = (object) $db->{$tableName}->findOne($criteria);
            } else {
                $item = iterator_to_array($db->{$tableName}->find($criteria)->skip($from)->limit($to)->sort($order));
                $item = My_Controller_Action_Helper_Util::array2object($item);
            }
        }

        if (is_object($item)) {
            $noTablename === false ? $item->table_name = $tableName : null;
            return $noBaseclass === true ? $item : new self($item);
        } else {
            return false;
        }
    }


    public static function toURL($string) {
        $new = array(' ', 	'[', 	']', 	'%',	',',	'.',	'�', 	'$',	'!',	'&',	'/',	'\\',	'?',	'+',	'(',	')',	':',	'"',	'--',	"'", "\x99", "\xA9", "\xAE", "\xA0");
        $old = array('-', 	'',		'',		'',		'',		'-',	'',		'', 	'',		'and',	'or',	'or',	'',		'',		'',		'',		'',	'',	'-', 	'', '', '', '', '');
        $string = str_replace($new, $old, $string);
        return strtolower($string);
    }

}