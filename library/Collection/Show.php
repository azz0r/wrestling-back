<?php


class Collection_Show extends Collection_Base {


    public static function appendAutomaticFields($document) {
        $document->title_url = parent::toURL($document->title);
        $document->image = parent::toURL($document->title).'.png';
        return $document;
    }

    
}