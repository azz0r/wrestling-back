<?php


class System_Define
{


    public function __construct() {
        define('FACEBOOK_PROXY', 	'https://fbproxy.theaudience.com:9443/');
        define('TWITTER_PROXY', 	'https://fbproxy.theaudience.com:9443/twitter/');
        define('GOOGLE_PROXY', 		'https://profiles.google.com/');
        define('GRAVATAR_URL',		'https://secure.gravatar.com/');
        define('COUNT_HIGH',		999999); /*used for returning everything high, do no 200 limit on a fetch for example*/

        date_default_timezone_set('Europe/London');
        setlocale(LC_ALL, array('en_GB', 'English'));

    }


}