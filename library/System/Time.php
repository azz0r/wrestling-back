<?php

#$sGMTMySqlString = gmdate("Y-m-d H:i:s", $tUnixTime);
#TimeAgo - http://drupal.org/node/61565#comment-198230

class System_Time {


    public static function DateTime_To_TimeStamp($str) {
        list($date, $time) = explode(' ', $str);
        list($year, $month, $day) = explode('-', $date);
        list($hour, $minute, $second) = explode(':', $time);
        return mktime($hour, $minute, $second, $month, $day, $year);
    }

    public static function make_timezone_list($timestamp, $output='jS M H:i') {

        $return		= array();
        $date 		= new DateTime(date("Y-m-d H:i:s", $timestamp));
        $timezones 	= array(
            'GMT' => 'GMT',
            'CET' => 'CET',
            'EST' => 'EST',
            'PST' => 'PST'
        );

        foreach ($timezones as $timezone => $code) {
            $date->setTimezone(new DateTimeZone($code));
            $return[$timezone] = $date->format($output);
        }
        return $return;
    }


    public static function Date_To_TimeStamp($str) {
        list($year, $month, $day) = explode('-', $str);
        return mktime(0, 0, 0, $month, $day, $year);
    }


    public static function TimeStamp_To_DateTime($timestamp) {
        return date("Y-m-d H:i:s", $timestamp);
    }


    public static function TimeStamp_To_Date($timestamp) {
        return date("Y-m-d", $timestamp);
    }


    public static function TimeAgo($timestamp) {

        $uptime 	= time() - $timestamp;
        $year 		= floor($uptime / 31104000);
        $month 		= floor(($uptime % 31104000) / 2592000);
        $day 		= floor((($uptime % 31104000) % 2592000) / 86400);

        $hour 		= floor(((($uptime % 31104000) % 2592000) % 86400) / 3600);
        $minute 	= floor((((($uptime % 31104000) % 2592000) % 86400) % 3600) / 60);
        $second 	= floor((((($uptime % 31104000) % 2592000) % 86400) % 3600) % 60);

        if ($year) {
            return "$year year".(($year > 1) ? 's' : NULL);#, $month month";
        } else if  ($month and !$year) {
            return "$month month".(($month > 1) ? 's' : NULL);#, $day day";
        } else if  ($day and !$month) {
            return "$day day".(($day > 1) ? 's' : NULL);#, $hour hour";
        } else if  ($hour and !$day) {
            return "$hour hour".(($hour > 1) ? 's' : NULL);#, $minute min";
        } else if  ($minute and !$hour) {
            return "$minute min".(($minute > 1) ? 's' : NULL);#, $second sec";
        } else if  ($second and !$minute) {
            return "$second sec".(($second > 1) ? 's' : NULL);
        } else {
            return "$second sec".(($second > 1) ? 's' : NULL);
        }
    }


    public function DaysAgo($start) {
        $finish			= date('m-d-Y');
        $date_parts1 	= explode('-', $start);
        $date_parts2 	= explode('-', $finish);

        $start_date 	= gregoriantojd($date_parts1[0], $date_parts1[1], $date_parts1[2]);
        $end_date 		= gregoriantojd($date_parts2[0], $date_parts2[1], $date_parts2[2]);
        return $end_date - $start_date;
    }


    public function MonthsAgo($start) {

        $finish			= date('m-d-Y');
        $date_parts1 	= explode('-', $start);
        $date_parts2 	= explode('-', $finish);

        $start_date 	= gregoriantojd($date_parts1[0], $date_parts1[1], $date_parts1[2]);
        $end_date 		= gregoriantojd($date_parts2[0], $date_parts2[1], $date_parts2[2]);

        $days 	= $end_date - $start_date;
        return round($days / 30);
    }


    public function HoursAgo($start) {
        $start			= strtotime($start);
        $finish			= time();
        $uptime			= $finish - $start;
        return round($uptime / 86400);
    }


    public function countdown($end_time) {

        $end_day 	= ($end_time - time())/86400;
        $end_day 	= floor($end_day);
        $end_hours 	= ($end_time - mktime(date('H'),date('i'),date('s'),date('m', $end_time),date('d', $end_time),date('y', $end_time)))/3600;

        if (abs($end_hours) != $end_hours){
            $end_hour = (24-date('H'))+date('H', mktime(date('H', $end_time),date('i', $end_time),date('s', $end_time),date('m', $end_time),date('d', $end_time),date('y', $end_time)));
            $end_hour = floor($end_hour);
        } else {
            $end_hour = floor($end_hours);
        }

        $end_minutes = ($end_time - mktime(date('H', $end_time),date('i'),date('s'),date('m', $end_time),date('d', $end_time),date('y', $end_time)))/60;

        if(abs($end_minutes) != $end_minutes){
            $end_minutes = (60-date('i'))+date('i', mktime(date('H', $end_time),date('i', $end_time),date('s', $end_time),date('m', $end_time),date('d', $end_time),date('y', $end_time)));
            $end_minutes = floor($end_minutes);
        } else {
            $end_minutes = floor($end_minutes);
        }

        $end_seconds = ($end_time - mktime(date('H', $end_time),date('i', $end_time),date('s'),date('m', $end_time),date('d', $end_time),date('y', $end_time)));

        if (abs($end_seconds) != $end_seconds){
            $end_seconds = (60-date('s'))+date('s', mktime(date('H', $end_time),date('i', $end_time),date('s', $end_time),date('m', $end_time),date('d', $end_time),date('y', $end_time)));
            $end_seconds = floor($end_seconds);
        } else {
            $end_seconds = floor($end_seconds);
        }

        return $end_day." Days, ".$end_hour." Hours, ".$end_minutes." Mins";// ".$end_seconds." Secs";

    }


    public static function GetMonthString($n) {
        return date("F", mktime(0, 0, 0, $n, 1, 2005));
    }


    public function get_weeks($amount=16) {

        $valid_dates 	= array();
        $date_counter 	= 0;
        $date_counter 	= strtotime("previous sunday"); // I love being a lazy bastard!
        $i 				= 0;

        while ($i < $amount) {
            array_push($valid_dates, $date_counter);
            $date_counter = strtotime("previous sunday 23:59:59", $date_counter);
            $i++;
        }

        foreach ($valid_dates as $date) {
            $values[$date]['start'] = $date;
            $values[$date]['end'] 	= $date + 604800;
        }

        return $values;
    }


    public function getMonthDateRange($month=01, $year=2013) {

        $amount = cal_days_in_month(CAL_GREGORIAN, $month, $year);

        $dates = array();
        $current = strtotime("$year-$month-01");
        $last = strtotime("$year-$month-$amount ");

        while ($current <= $last ) {
            $dates[] = date("Y-m-d", $current);
            $current = strtotime("+1 day", $current);
        }

        return $dates;
    }


    public static function getDays($amount=15) {

        $valid_dates = array();
        $date_counter = 0;
        $date_counter = strtotime("tomorrow 23:59:59");
        $i = 0;

        while ($i < $amount) {
            array_push($valid_dates, $date_counter);
            $date_counter = strtotime("tomorrow 23:59:59", $date_counter);
            $i++;
        }

        return $valid_dates;
    }


    public function get_days($amount=60) {

        $valid_dates = array();
        $date_counter = 0;
        $date_counter = strtotime("yesterday 23:59:59");
        $i = 0;

        while ($i < $amount) {
            array_push($valid_dates, $date_counter);
            $date_counter = strtotime("yesterday 23:59:59", $date_counter);
            $i++;
        }

        foreach ($valid_dates as $date) {
            $values[$date]['start'] = $date;
            $values[$date]['end'] 	= $date + 86399;
        }

        return $values;
    }
    public function dateToTimestamp($date=null) {
        list($year, $month, $day) = explode('-', $date);
        return mktime(0, 0, 0, $month, $day, $year);
    }


    public function mmddyyyyToMYSQL($date) {
        $date = str_replace('/', '-', $date);
        $explode = explode('-', $date);

        if (count($explode) != 3) {
            return false;
        } else {
            return $explode[2].'-'.$explode[0].'-'.$explode[1];
        }
    }

    public static function IsUserOnline($lastSeen) {
        return ( $lastSeen > (time() - 1200) ) ? 1 : NULL;
    }


    public static function LongAgo($timestamp) {

        $uptime 		= time() - $timestamp;
        $return[]		= array();

        $return['year']		= floor($uptime / 31104000);
        $return['month'] 	= floor(($uptime % 31104000) / 2592000);
        $return['day'] 		= floor((($uptime % 31104000) % 2592000) / 86400);

        $return['hour'] 	= floor(((($uptime % 31104000) % 2592000) % 86400) / 3600);
        $return['minute'] 	= floor((((($uptime % 31104000) % 2592000) % 86400) % 3600) / 60);
        $return['second'] 	= floor((((($uptime % 31104000) % 2592000) % 86400) % 3600) % 60);

        return $return;
    }


    public function checkDateFormat($date) {

        if (preg_match ("/^([0-9]{4})-([0-9]{2})-([0-9]{2})$/", $date, $parts)) {
            if (checkdate($parts[2],$parts[3],$parts[1])) {
                return true;
            }
            return false;
        }
        return false;
    }


    public static function validDate($date) {
        $date =  explode("-",$date);

        if (count($date) < 2) {
            return false;
    }
        list($yy,$mm,$dd) = $date;

        if (is_numeric($yy) && is_numeric($mm) && is_numeric($dd)) {
            return true;
        } else {
            return false;
        }
    }

}