<?php


class System_Model extends System_Controller {


    public $map;
    public $update;
    public $validationErrors = array();
    public $assert;
    public $errors;
    public $mongoDB;

    public $currentModel;
    public $currentObject;
    public $currentTableName;


    public function __construct($presets=array()) {

        /* include errors file*/
        $this->errors   = System_Errors::get();
        $map            = new System_Model_Map();
        $this->map      = $map->map;
        $this->assert   = new System_Assert();
        $this->validCastTypes = array('array', 'object', 'integer', 'string', 'boolean', 'bool', 'null', 'int');

        foreach ($presets as $key => $value) {
            $this->setKey($key, $value);
        }
        return $this;
    }


    public function setKey($key, $value) {
        $this->{$key} = is_array($value) ? (object) $value : $value;
    }


    public function getKey($key) {
        return $this->{$key};
    }


    public function get($document=null, $tableName=null) {
        $document       = is_null($document) ? $this->currentObject : $document;
        $tableName      = isset($tableName)  ? $tableName : $this->currentTableName;
        $options        = array('tableName' => $tableName, 'automatic' => true, 'hidden' => false);
        $document       = $this->createDocument($document, $options);
        return $document;
    }


    public function post($document=null) {
        $document           = is_null($document) ? $this->currentObject : $document;
        $options            = array('tableName' => $this->currentTableName, 'automatic' => false, 'hidden' => true, 'method' => 'post');
        $document           = $this->createDocument($document, $options);
        $document->created  = time();
        return $document;
    }


    public function put($document, $originalDocument) {
        $options            = array('tableName' => $this->currentTableName, 'automatic' => false, 'hidden' => true, 'method' => 'put');
        $document           = $this->createDocument($document, $options);
        $document->updated  = time();
        $document->created  = isset($originalDocument->created) ? $originalDocument->created : time();
        return $document;
    }


    public function createDocument($document, $options=array()) {

        $tableName = isset($options['tableName'])  ? $options['tableName'] : $this->currentTableName;

        if (empty($tableName)) {
            throw new Exception('Table name is empty on createDocument, cannot continue');
        }

        $hidden                 = isset($options['hidden'])     ? $options['hidden']    : true;
        $automatic              = isset($options['automatic'])  ? $options['automatic'] : true;
        $subObject              = isset($options['subObject'])  ? $options['subObject'] : false;
        $type                   = isset($options['type'])       ? $options['type']      : 'fields';
        $method                 = isset($options['method'])     ? $options['method']    : 'get';
        $removeEmptySubObject   = isset($options['remove_empty_sub_object'])  ? $options['remove_empty_sub_object'] : true;

        $newDocument    = new stdClass();
        $document       = (object) $document;

        $this->_log("createDocument: $tableName / hidden: ".(int)$hidden." / automatic: ".(int)$automatic." / sub object: ".(int)$subObject);
        $this->_log("Document is: ".print_r($document,true));

        foreach ($this->map[$tableName][$type] as $key => $value) {
            $shortKey = $this->map[$tableName][$type];
            $this->_log('looping at '.$key);

            /* if we don't want automatic values and its automatic, skip this */
            if ($automatic === false && isset($value['automatic']) && $value['automatic'] === true) {
                $this->_log('automatic field, skipping, '.$key);
                continue;
            }

            if ( ( $method === 'post' && isset($shortKey['post']) && !isset($document->$key) ) || ( $method === 'put' && isset($shortKey['put']) && !isset($document->$key) ) ) {
                $newDocument->$key = null;
                continue;
            }

            /* if we don't want hidden values, then skip if its hidden */
            if ($hidden === false && isset($value['hidden']) && $value['hidden'] === true) {
                $this->_log('hidden field, skipping, '.$key);
                continue;
            }

            /* what is the current value on the document */
            $currentValue = isset($document->$key) ? $document->$key : null;
            $this->_log('currentValue is '.print_r($currentValue,true));

            /* preset the item on the object */
            $newDocument->$key = null;

            /* if its an array of objects */
            if ($value['type'] == 'array-objects') {
                $this->_log($key.' is an array of objects');
                /*preset it to false*/
                $newDocument->$key = array();

                if (!empty($document->$key)) {
                    $returns = array();
                    foreach ($document->$key as $subKey => $subValue) {

                        /* if the object isn't null then we can send it to reloop itself */
                        if (!is_null($currentValue) && !isset($value['map']) && isset($value['table_name'])) {
                            $childOptions        = array(
                                'automatic'     => $automatic,
                                'hidden'        => $hidden,
                                'tableName'     => $value['table_name'],
                                'subObject'     => true,
                                'method'        => $method
                            );

                            $returns[]  = $this->createDocument($subValue, $childOptions);
                        }
                    }
                    $newDocument->$key = $returns;
                }

                /* its not an object, so we pass it to createKey */
            }


            /* if the key is an object, lets pass it back through this */
            else if ($value['type'] == 'object') {
                $this->_log($key.' is an object');
                /*preset it to false*/
                $newDocument->$key = false;

                if (!empty($document->$key)) {

                    /* if the object isn't null then we can send it to reloop itself */
                    if (!is_null($currentValue) && !isset($value['map']) && isset($value['table_name'])) {
                        $childOptions        = array(
                            'automatic'     => $automatic,
                            'hidden'        => $hidden,
                            'tableName'     => $value['table_name'],
                            'subObject'     => true,
                            'method'        => $method
                        );

                        $newDocument->$key   = $this->createDocument($document->$key, $childOptions);

                        if ($removeEmptySubObject === true && !empty($newDocument->$key)) {
                            $newDocument->$key = $this->removeEmptySubObject($newDocument->$key, $value['table_name']);
                        }
                    }
                }

            /* its not an object, so we pass it to createKey */
            } else if (isset($document->$key)) {
                $this->_log('its a default key'. $key);
                $newDocument->$key  = $this->createKey($document->$key, $value);
            } else {
                $newDocument->$key = $this->createKey(null, $value);
            }

        }
        $newDocument = $this->appendAutomaticFields($newDocument, $tableName);

        $this->_log("output is ".print_r($newDocument,true));
        return $newDocument;
    }


    /*backwards compatible wrapper, cuts out if isset items */
    public function getCleanValue($key, $type, $options=array()) {

        $tableName = isset($options['tableName'])   ? $options['tableName'] : $this->currentTableName;
        $value     = isset($options['value'])      ? $options['value']     :
            (isset($this->currentObject->$key) ? $this->currentObject->$key : null);

        if (isset($this->map[$tableName][$type][$key])) {
            $shortKey = $this->map[$tableName][$type][$key];
        } else {
            $shortKey = array();
        }

        return $this->createKey($value, $shortKey);
    }


    public function createKey($value, $options) {
        $this->_log("createKey for the value: ".print_r($value,true));

        /* if we have a default value, and the POST/GET request doesn't have it, set it to the default value */
        if (isset($options['default'])) {
            $this->_log("we have a default option, before: ".print_r($value,true));
            $value = !is_null($value) && !empty($value) && ($value != '') ? $value : $options['default'];
            $this->_log("we have a default option, after: ".print_r($value,true));
        }

        /* if we have defined a max length of a field then make sure its no longer */
        if (isset($value) && isset($options['max']) && ($value > $options['max'])) {
            $this->_log("we have a max length, before: ".print_r($value,true));
            $value = substr($value, 0, (int) $options['max']); /* untested, please test this */
            $this->_log("we have a max length, after: ".print_r($value,true));
        }

        /* make sure our field adheres to the length restriction */
        if (isset($options['length']) && !is_array($value) && !is_object($value)) {
            $this->_log("we have a length, before: ".print_r($value,true));
            $value = substr($value, 0, (int) $options['length']);
            $this->_log("we have a length, after: ".print_r($value,true));
        }

        /* if an options array is present, lets check our value is in those options, if it isnt, then we use the default value */
        if (isset($options['options']) && is_array($options['options'])) {
            $this->_log("we have a options, before: ".print_r($value,true));
            if (isset($value) && !in_array($value, $options['options'])) {
                $value = $options['default'];
            } else if (isset($options['default']) && is_null($value)) {
                $value = $options['default'];
            }
            $this->_log("we have a length, after: ".print_r($value,true));
        }

        /* if the item we're checking is on the POST/GET request, but it isnt the same type (aka int/string/bool), then set it to that type */
        if (isset($options['type']) ){
            $this->_log("we have a type, before: ".print_r($value,true));
            if (in_array($options['type'], $this->validCastTypes)) {
                settype($value, $options['type']);
            }
            $this->_log("we have a length, after: ".print_r($value,true));
        }

        /* if defaulting back to zero isn't acceptable */
        if (isset($options['zero']) && $options['zero'] === false && $value === 0) {
            $this->_log("we have a zero, before: ".print_r($value,true));
            settype($value, 'null');
            $this->_log("we have a zero, after: ".print_r($value,true));
        }

        $this->_log("end of createKey, result is:  ".print_r($value,true));
        return $value;
    }


    public function validateDocument($document, $tableName=null) {

        $tableName          = is_null($tableName) ? $this->currentTableName : $tableName;
        $validationErrors   = array();
        $document           = (object) $document;

        foreach ($this->map[$tableName]['fields'] as $key => $value) {
            if (isset($value['validate'])) {
                foreach ($value['validate'] as $validationKey => $validate) {
                    $documentKey = isset($document->$key) ? $document->$key : null;
                    $validQuery = $this->assert->$validate($documentKey);
                    if ($validQuery == false) {
                        $validationErrors[$key][$validationKey] = $this->errors[$validationKey]['message'];
                    }
                }
            }
        }
        return $validationErrors;
    }


    public function removeEmptySubObject($document, $tableName) {
        $nulls = 0;
        foreach ($this->map[$tableName]['fields'] as $key => $value) {
            if (!isset($document->{$key}) ||
                /* If the empty model has array fields appended automatically lets count them towards the nulls */
                (isset($document->{$key}) && (is_null($document->$key) || is_array($document->$key) && empty($document->$key)))) {
                $document->{$key} = null;
                $nulls++;
            }
        }
        /* the amount of nulls is the same as the number of fields on the object, so we set the object to null */
        if ($nulls == count((array)$document)) {
            return false;
        } else {
            return $document;
        }
    }


    public function appendAutomaticFields($document, $tableName) {
        return (class_exists("Collection_{$tableName}", true))
            ? call_user_func_array(array("Collection_{$tableName}", "appendAutomaticFields"), array($document)) : $document;
    }


    public function _log($log) {
        //echo print_r($log,true).'<br />';
    }


    public function updateRelatedTables($document = null, $tableName = null) {

        $tableName = is_null($tableName) ? $this->currentTableName : $tableName;
        $document = is_null($document) ? $this->currentObject : $document;

        switch ($tableName) {
            case 'Client':
                //update Post
                $this->mongoDB->Post->update(
                    array('client_id' => (int)$document->id),
                    array('$set' => array('client' => $document)),
                    array('multiple' => true)
                );
                //update Post->Image->Clients.$             // very tricky
                $clientsToUpdate = iterator_to_array($this->mongoDB->Post->find(array('client_id' => (int)$document->id)));
                $clients = array();
                foreach ($clientsToUpdate as $key => $client) {
                    $clients[] = $document;
                }
                $this->mongoDB->Post->update(
                    array('client_id' => (int)$document->id),
                    array('$set' => array('image.clients' => $clients)),
                    array('multiple' => true)
                );
                //update Image
                $this->mongoDB->Image->update(
                    array('clients.id' => (int)$document->id),
                    array('$set' => array('clients.$' => $document)),
                    array('multiple' => true)
                );
                break;
            case 'Image':
                //update Image
                $this->mongoDB->Post->update(
                    array('image_id' => (int)$document->id),
                    array('$set' => array('image' => $document)),
                    array('multiple' => true)
                );

            case 'User':
                //update User
                $this->mongoDB->Post->update(
                    array('user_id' => (int)$document->id),
                    array('$set' => array('user' => $document)),
                    array('multiple' => true)
                );
                //update Comments
                $this->mongoDB->Comment->update(
                    array('user_id' => (int)$document->id),
                    array('$set' => array('user' => $document)),
                    array('multiple' => true)
                );

                break;

            default:
                break;
        }
    }

    public function deleteRelatedTables($document = null, $tableName = null) {

        $tableName = is_null($tableName) ? $this->currentTableName : $tableName;
        $document = is_null($document) ? $this->currentObject : $document;

        switch ($tableName) {
            case 'Client':
                //delete Post
                $this->mongoDB->Post->remove(
                    array('client_id' => (int)$document->id)
                );
                //delete Image
//                $this->mongoDB->Image->remove(
//                    array('clients.id' => (int)$document->id)
//                );
                //delete Client Platform
                $this->mongoDB->Client_Platform->remove(
                    array('client_id' => (string)$document->id)
                );
                break;
//            case 'Image':
//                //remove Image
//                $this->mongoDB->Post->remove(
//                    array('image_id' => (int)$document->id)
//                );

            case 'User':
                //remove User
                $this->mongoDB->Post->remove(
                    array('user_id' => (int)$document->id)
                );
                //remove comments
                $this->mongoDB->Comment->remove(
                    array('user_id' => (int)$document->id)
                );

                break;

            default:
                break;
        }
    }



}