<?php


class System_Controller {


    public $rest;
    public $user;
    public $method;
    public $variables;

    /* mongo controller items*/
    public $filters;
    public $results;
    public $model;

    protected $status;
    protected $error;
    protected $errorData;
    protected $db;
    protected $skipAuthentication = false;
    protected $auth;
    protected $administrator = false;

    public $perPage 	= 100;
    public $maxPerPage 	= 100;

    protected $benchmarkStart;


    public function __construct($rest) {
        $this->rest 	= $rest;
        $this->method 	= $this->rest->getMethod();
        $this->auth = System_Authentication_Init::getInstance();

        $mongoDB        = new System_Database_Mongo();
        $this->mongoDB  = $mongoDB->getConnection();

        //manages assert and map
        $this->model = new System_Model(array('mongoDB' => $this->mongoDB));

        $this->results = array();
        $this->setStatus();
    }


    public function init() {}


    /*turns an array into an object
     * @todo is this used? 
     * */
    public function object($array=array()) {
        return new OpenSource_Class($array);
    }


    public function internalCall($options=array()) {

        $method         = isset($options['method']) ? $options['method'] : 'get';
        $controllerName = 'Controller_User_Index';

        if (isset($options['controller_name'])) {
            $controllerName = $options['controller_name'];
        } else if (isset($options['controller'])) {
            $controllerName = "Controller_".$options['controller'];
        }

        if (isset($options['id'])) {
            $options['params'] = array('id' => $options['id']);
        }

        $rest = clone ($this->rest);
        $rest->clearParams();
        $rest->setMethod($method);

        if (isset($options['params'])) {
            foreach ($options['params'] as $key => $value) {
                $rest->setParam($method, $key, $value);
            }
        }

        $document           = new $controllerName($rest);
        $returnedDocument   = $document->executeClean();

        if ($error = $document->getInternalError()) {
            return $error;
        } else {
            return $returnedDocument;
        }
    }



    public function logAction($controller, $action, $before=array(), $after=array(), $objectId=null) {
        /* log action is low priority, so lets catch anything going wrong */
        try {
            $insert = array(
                'user_id' 		=> $this->user->id,
                'controller' 	=> $controller,
                'action' 		=> $action,
                'object_id' 	=> $objectId,
                'before' 		=> $before,
                'after' 		=> $after,
                'user'          => $this->user,
                'enabled' 		=> true,
                'updated' 		=> null,
                'created' 		=> time()
            );

            $this->mongoDB->Log_Action->insert($insert);
            return $insert;

        } catch (Exception $e) {
            //do nothing
        }
    }



    /**
     * Overwrite auth object
     * @param Model_Authenticate_Init $auth
     */
    /* @todo seperate with _ */
    public function setAuth($auth) {
        $this->auth             = $auth;
        $this->user             = $this->auth->getUserObject();
        $this->setAdministrator();
    }


    /*replacement by daniel, due to isset problems*/
    public function returnClean($object, $fields) {
        foreach ($object as $key => $value) {
            if (!in_array($key, $fields)) {
                unset($object->{$key});
            }
        }
        return $object;
    }


    /* Returns db adapter */
    public function getAdapter() {
        return $this->_db;
    }


    public function setStatus($status = 200) {
        $this->status = $status;
    }


    public function setError($error = 0, $data=array()) {
        $this->error = $error;
        $this->errorData = $data;
        return array('message' => $error, 'data' => $data);
    }


    public function getInternalError() {

        $errors = System_Errors::get();
        $return = array('errors'=>array());

        if (is_numeric($this->error) && $this->error > 0) {
            $id = $this->error;
            $error 	= isset($errors[$id]) ? $errors[$id]['message'] : $id.': Error Code Is Currently Not Recorded';
            $status = isset($errors[$id]) ? $errors[$id]['header'] : 500;
            $return['errors'] = array('code' => $id, 'message' => $error, 'data' => $this->errorData);
            return $return;
        } else {
            return false;
        }
    }


    /**
     * Sets the skipAuth flag
     */
    public function skipAuthentication() {
        $this->skipAuthentication = true;
    }


    /* Authenticates and sets the main user auth data */
    public function auth($auth = null) {
        // no need to include the rest in auth, we have it already in public/index.php
        $this->auth->authenticate();
        $this->user = $this->auth->getUserObject();
        $this->setAdministrator();
    }


    public function executeClean($data=array()) {

        $this->init();

        if (!$this->skipAuthentication) {
            $this->auth();
        }

        $this->variables = $this->_getVars($this->method);

        $data = $this->{$this->method}();

        if (!empty($data)) {
            return $data;
        } else {
            return NULL;
        }
    }


    /* Executes rest method. Additionally authenticate if we earlier haven't skipped auth.*/
    public function execute() {
        global $apiStartTime;

        $this->init();

        if (!$this->skipAuthentication) {
            $this->auth();
        }

        if (method_exists($this, $this->method) === false) {
            return $this->rest->setErrorData(777);
        }

        /*set variables to a global $this->variables*/
        $this->variables = $this->_getVars($this->method);

        $data = $this->{$this->method}();

        if (!isset($_GET['debug'])) {

            $return = array();
            $return['status_code'] 		= $this->status;
            $return['administrator'] 	= $this->administrator;

            if ($this->administrator) {
                /*cache outputs*/
                if (isset($_SESSION['temp']['cache']['outputs']) && !empty($_SESSION['temp']['cache']['outputs'])) {
                    $return['cache'] = array();
                    $return['cache']['hits'] 			= count($_SESSION['temp']['cache']['outputs']);
                    $return['cache']['hits_outputs']	= $_SESSION['temp']['cache']['outputs'];
                    $return['cache']['writes'] 			= count($_SESSION['temp']['cache']['writes']);
                    $return['cache']['writes_output']	= $_SESSION['temp']['cache']['writes'];
                }
                /*logging*/
                $return['logged_messages'] = $_SESSION['temp']['logged_messages'];
            }

            /*Set User*/
            if ($this->user){//maybe we skipped auth
                $userObject = clone $this->user;
                unset($userObject->encrypted_password);
                $return['user'] = $userObject;
            }

            /*load time*/
            $time = explode(' ', microtime());
            $finishApiTime = $time[1] + $time[0];
            $return['load_time'] = round(($finishApiTime - $apiStartTime), 4);

            $return['memory_usage'] = $this->_convertBytes(memory_get_usage());
            /*set data*/
            $data = array('data' => empty($data) ? array() : $data, 'debug' => $return);
        }

        System_Authentication_Session::cleanDebugOutput();

        if ($data!==null) {
            $this->rest->setOutputData($data);
        } else {
            $this->rest->setOutputData(array());
        }

        if ($this->error != 0) { #if an error has been set, we set the rest object to handle it, otherwise we use the status set, should be 200!
            $this->rest->setErrorData($this->error, $this->errorData);
        } else {
            $this->rest->setOutputResponse($this->status);
        }
    }


    private function _convertBytes($size) {
        $unit=array('b','kb','mb','gb','tb','pb');
        return @round($size/pow(1024,($i=floor(log($size,1024)))),2).' '.$unit[$i];
    }


    public function getRest() {
        return $this->rest;
    }


    /* Returns a response from the rest */
    public function send() {
        return $this->rest->sendResponse();
    }


    public function _getParam($param, $fallback=NULL) {
        return $this->rest->_getParam($param, $fallback);
    }


    public function _getVars($type=NULL) {
        return $this->rest->getRequestVars($type);
    }


    protected function setAdministrator() {
        $this->administrator = false;

        return true;
    }


    public function benchmarkStart() {
        $time = microtime();
        $time = explode(' ', $time);
        $time = $time[1] + $time[0];
        $this->benchmarkStart = $time;
    }


    public function benchmarkEnd() {
        //benchmark end
        $time = microtime();
        $time = explode(' ', $time);
        $time = $time[1] + $time[0];
        $finish = $time;

        if (isset($this->benchmarkStart) && !empty($this->benchmarkStart)) {
            return round(($finish - $this->benchmarkStart), 4);
        } else {
            return false;
        }
    }



    /* MONGO CONTROLLERS *
	/* we create our own ids for mongo _id and id field */
    public function generateId($tableCollection) {
        /* this is a temporary solution - @todo
         * we retrieve the last row of the collection passed and return it +1
         * this is because 32php makes a large int -2342423 if its over the threshold.
         */
        $lastRow = $tableCollection->find()->sort(array('id' => -1))->limit(1);
        if (!empty($lastRow)) {
            foreach ($lastRow as $row) {
                return (int) $row['id'] + 1;
            }
        }
        return 1;
        /* we cant do this with 32 bit PHP, switch back to this when we have it */
        #return $this->user->id.time();
        throw new Exception ("We couldn't generate a unique id");
    }


    /* get the default filters we need for paging, and sorting */
    public function getDefaultFilters($tableName) {

        /* set the filters for the controller */
        $this->filters = new stdClass();

        /* make default filters just incase */
        $defaultFilters = array(
            'from'          => array('type' => 'integer', 'default' => 0),
            'to'            => array('type' => 'integer', 'default' => 15, 'max' => 500),
            'order_by'      => array(
                'default' => 'id',
                'type' => 'string',
                'options' => array('id', 'alias', 'status', 'updated', 'created')
            ),
            'order_direction' => array(
                'default' => 'DESC',
                'type' => 'string',
                'options' => array('ASC', 'DESC')
            )
        );
        $filters = isset($this->model->map[$tableName]['filters']) ? $this->model->map[$tableName]['filters'] : $defaultFilters;
        /*end making filters */

        /* to cut down code we have keys => value */
        $keys = array('from' => 'from', 'to' => 'to', 'order_direction' => 'orderDirection', 'order_by' => 'orderBy');

        /* loop through the keys and values and set the filters */
        foreach ($keys as $key => $value) {
            $inputValue = isset($this->variables[$key]) ? $this->variables[$key] : null;
            $this->filters->$value = $this->model->createKey($inputValue,
                (isset($filters[$key]) ? $filters[$key] : $defaultFilters[$key]));
        }
    }


    /*used to figure out the users id, either by checking the data sent (post/put/delete/get request) or by checking the URL */
    public function _getId() {

        $id = $this->_getParam('id', $this->_getParam('_id'));

        //the get id wasnt set so check for a put id
        if (is_null($id)) {
            if (isset($this->_variables['id'])) {
                return $this->_variables['id'];
            } else if (isset($this->_variables['_id'])) {
                return $this->_variables['_id'];
            } else {
                return false;
            }
        } else {
            return $id;
        }
    }

    /* END MONGO CONTROLLERS */



    public function stripExcess($document) {
        if (isset($this->variables['_response']) && $this->variables['_response'] != 'true') {
            $newDocument = new stdClass();
            $response = explode(',', $this->variables['_response']);
            foreach ($response as $key) {
                if (isset($document->$key)) {
                    $newDocument->$key = $document->$key;
                }
            }
            return $newDocument;
        }
        return $document;
    }

}