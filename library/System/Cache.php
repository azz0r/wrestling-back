<?php


class System_Cache {


	//cache object
	public $cache;

	//debug or enable
	public $enable 	= true;	
	public $debug 	= false;

	//name and md5 of it
	public $name;
	public $nameMD5;	
	public $tags = array();
	public $length = 7200;
	//result object
	public $results;

	//benchmark
	public $benchmarkStart;
	public $benchmarkEnd;


	public function getCacheOutput() {

		if (isset($this->debug) && !empty($this->debug)) {
			return $this;
		} else {
			

			if (isset($this->results) && !empty($this->results)) {
				$_SESSION['temp']['cache']['outputs'][] = $this->results;
				return $this->results;
			} else {
				return false;
			}
		}
	}


	public function init($enable=false) {

		$this->enable = $enable;

		if ($enable == true) {	
			$this->benchmarkStart	= $this->getBenchmarkStart();	
			$this->cache 			= $this->getCache();
			$this->nameMD5			= $this->getNameMD5();
			$this->results			= $this->getCacheLoad();
		} else {
			return false;
		}
	}


	public function getCache() {

		try {
			if (!isset($this->cache) || empty($this->cache)) {
				$front = array(
					'lifetime' 					=> 1800,
					'automatic_serialization' 	=> true,
					'caching' 					=> true,
					'cache_id_prefix' 			=> VERSION.'_',
					'debug_header' 				=> false, 
					'default_options' 			=> array (
						'cache_with_get_variables' 		=> true,
						'cache_with_post_variables'		=> false,
						'cache_with_session_variables' 	=> false,
						'cache_with_cookie_variables'	=> false
					),
				);
				$back = array('cache_dir' => _CACHE);
				$cache = Zend_Cache::factory('Page', 'File', $front, $back);
				$cache->start();
				Zend_Registry::set('cache', $cache);
				return $cache;
			}
		} catch (Exception $e) {
			$this->enable = false;
			return false;
		}
		
	}	
	
	public function getNameMD5() {
		if (!isset($this->nameMD5) || empty($this->nameMD5)) {
			if (isset($this->name) && !empty($this->name)) {
				if (is_array($this->name)) {
					$this->name = implode('|', $this->name);
				} 
				return System_URL::cache(Md5($this->name));
			}
		} else {
			return $this->nameMD5;
		}
	}


	public function getCacheLoad() {
		if (isset($this->cache) && !empty($this->cache) && isset($this->nameMD5) && !empty($this->nameMD5)) {
			return $this->cache->load($this->nameMD5);	
		} else {
			return false;
		}
	}


	public function getSaved() {
		if (isset($this->cache) && !empty($this->cache) && 
		   isset($this->results) && !empty($this->results) && 
		   isset($this->name) && !empty($this->name) && 
		   isset($this->tags) && !empty($this->tags)) {
			$_SESSION['temp']['cache']['writes']++;
			$_SESSION['temp']['cache']['writes_outputs'][] = $this->results;
			return $this->cache->save($this->results, $this->nameMD5, $this->tags, $this->length);
		} else {
			return false;
		}
	}


	public function deleteAll() {
		if (isset($this->cache)) {
			return $this->cache->clean(Zend_Cache::CLEANING_MODE_ALL);
		} else {
			return false;
		}
	}


	public function deleteByTag() {
		return $this->getCacheTagClean();
	}
	public function getCacheTagClean() {
		if (isset($this->cache) && !empty($this->cache) && isset($this->tags) && !empty($this->tags)) {
			return $this->cache->clean(Zend_Cache::CLEANING_MODE_MATCHING_TAG,$this->tags);	
		} else {
			return false;
		}		
	}
	
	public function setLength($length) {
		$this->length = $length;
	}


	public function getBenchmarkStart() {
		//benchmark start
		$time = microtime();
		$time = explode(' ', $time);
		$time = $time[1] + $time[0];
		return $time;
	}


	public function setBenchmarkEnd() {
		//benchmark end
		$time = microtime();
		$time = explode(' ', $time);
		$time = $time[1] + $time[0];
		$finish = $time;

		if (isset($this->benchmarkStart) && !empty($this->benchmarkStart)) {
			$totalTime = round(($finish - $this->benchmarkStart), 4);
			$this->benchmarkEnd = 'Page generated in '.$totalTime.' seconds';	
		}
	}


}