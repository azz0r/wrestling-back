<?php


class System_Image {


	public $devices = array(
		'asset-thumb'		=> array('width' => 150, 'height' => 150, 'crop_ratio' => 1),
		'facebook-thumb'	=> array('width' => 300, 'height' => 300, 'crop_ratio' => NULL),
		'default'			=> array('width' => 720, 'height' => 960, 'crop_ratio' => NULL)
	);

	public $randomdir;
	public $fullNewImagePaths = array();
	private $fileinfo;
	public $copy = false;


	public function __construct($fileinfo) {
		$this->randomdir 	= date("Y-m");
		$this->fileinfo 	= $fileinfo;
	}

	
	public function setCopy($copy=true) {
		$this->copy = $copy;
	}
	public function setOriginalFilename($originalFilename='') {
		$this->originalFilename = $originalFilename;
	}


	public function processImage() {

		if ($this->copy === true) {
			return $this->moveImages();
		} else {
			if($this->uploadFile()) {
				$this->resizeFiles();
				return true;
			} else {
				throw new Exception('Couldnt process image');
				return false;
			}
		}
	}


	public function overwrite_devices($devices){
		$this->devices = $devices;
	}


	public function get_devices() {
		return $this->devices;
	}


	public function getFullNewImagePaths() {
		return $this->fullNewImagePaths;
	}


	public function moveImages() {

		/*loop through device versions of the image*/
		foreach($this->devices as $device => $res) {
			/*make directories for the images*/
			@mkdir(IMAGE_DIR.$device . '/' . $this->randomdir.'/');
			@mkdir(IMAGE_DIR.$device . '/' . $this->randomdir.'/'.date('d').'/');

			/*copy images to new directory*/
			copy(IMAGE_DIR.'tmp/'.$device.'_'.$this->originalFilename, IMAGE_DIR.$device.'/'.$this->randomdir.'/'.date('d').'/'.$this->fileinfo['name']);

			/*store the path on an array*/
			$this->fullNewImagePaths[$device] = array('file' => IMAGE_DIR.$device.'/'.$this->randomdir.'/'.date('d').'/'.$this->fileinfo['name'], 'path' => '/'.$device.'/'.$this->randomdir.'/'.date('d').'/'.$this->fileinfo['name']);

			/*remove the tmp image*/
			unlink(IMAGE_DIR.'tmp/'.$device.'_'.$this->originalFilename);
		}

		/*do the same for the original image*/
		@mkdir(IMAGE_DIR.'original/' . $this->randomdir);
		@mkdir(IMAGE_DIR.'original/' . $this->randomdir.'/'.date('d').'/');
		$copy = copy(IMAGE_DIR.'tmp/'.$this->originalFilename, IMAGE_DIR.'original/'.$this->randomdir.'/'.date('d').'/'.$this->fileinfo['name']);
		$this->fullNewImagePaths['original'] = array('file' => IMAGE_DIR.'original/'.$this->randomdir.'/'.date('d').'/'.$this->fileinfo['name'], 'path' => '/original/'.$this->randomdir.'/'.date('d').'/'.$this->fileinfo['name']);
		unlink(IMAGE_DIR.'tmp/'.$this->originalFilename);

		/*return status of action so far*/
		return $copy;
	}


	private function uploadFile() {
		@mkdir(IMAGE_DIR . 'original/' . $this->randomdir);
		@mkdir(IMAGE_DIR . 'original/' . $this->randomdir.'/'.date('d'));
		$this->fullNewImagePaths['original'] = array('file' => IMAGE_DIR.'original/'.$this->randomdir.'/'.date('d').'/'.$this->fileinfo['name'], 'path' => '/original/'.$this->randomdir.'/'.date('d').'/'.$this->fileinfo['name']);
		return rename($this->fileinfo['tmp_name'], IMAGE_DIR.'original/'.$this->randomdir.'/'.date('d').'/'.$this->fileinfo['name']);		
	}


	private function resizeFiles() {

		require_once _LIBRARY.'/Thumb/ThumbLib.inc.php';

		foreach($this->devices as $device => $res) {
			try {
				@mkdir(IMAGE_DIR . $device . '/' . $this->randomdir);
				@mkdir(IMAGE_DIR . $device . '/' . $this->randomdir.'/'.date('d'));
				$thumb = PhpThumbFactory::create(IMAGE_DIR.'original/'.$this->randomdir.'/'.date('d').'/'.$this->fileinfo['name']);

				if (is_null($res['crop_ratio'])) {
					$thumb->resize($res['width'],$res['height']);
				} else {
					$thumb->adaptiveResize($res['width'],$res['height']);
				}

				$format = $thumb->getFormat();
				$thumb->save(IMAGE_DIR.$device.'/'.$this->randomdir.'/'.date('d').'/'.$this->fileinfo['name'], $format);
				$this->fullNewImagePaths[$device] = array('file' => IMAGE_DIR.$device.'/'.$this->randomdir.'/'.date('d').'/'.$this->fileinfo['name'], 'path' => '/'.$device.'/'.$this->randomdir.'/'.date('d').'/'.$this->fileinfo['name']);
	

			} catch (Exception $e) {
				//nothing thanks
			}
		}
		return true;
	}


	public function bindPaths($object) {

		$url 						= isset($object->cloud) && $object->cloud == 1 ? ASSET_CLOUD_PUBLIC_URL : IMAGE_LINK;
		$this->devices['original'] 	= array();

		foreach ($this->devices as $key => $device) {
			$object->paths->$key = $url.'/'.$key.'/'.$object->path.'/'.$object->filename;
		}

		return $object;
	}


	public static function getPublicUrls($cloud, $path, $filename) {

		$devices 				= new System_Image(array());
		$devices 				= $devices->devices;
		$devices['original'] 	= array();
		$return  				= array();
		$url 					= ($cloud == 1) ? ASSET_CLOUD_PUBLIC_URL : IMAGE_LINK;

		foreach ($devices as $key => $device) {
			$return["_".str_replace('-', '', $key)] = $url.'/'.$key.'/'.$path.'/'.$filename;
			$return[$key] = $url.'/'.$key.'/'.$path.'/'.$filename;
		}

		return $return;
	}


	public static function getByClientIds($clientIds, $where = '', $from, $to, $count=0) {

		$db = System_Database::GetInstance();

		if (!is_array($clientIds)) {
			$clientIds = array($clientIds);
		}

		$originalClientIds = $clientIds;//because sort loses ['all']
		$calledBy 	= get_called_class();

		sort($clientIds);
	
		/*count*/
		if ($count == 1) {
			$from = 0;
			$to = COUNT_HIGH;
			$select = ' COUNT(*) as `count` ';
		} else {
			$select = ' d.* ';
		}
	
		if (count($clientIds) == 1) {
	
			if (!isset($originalClientIds['all'])) {
				$subWhere = ' ac.client_id in (\''. implode(',',$clientIds) . '\') AND ';
			} else {
				$subWhere = NULL;
			}
			$queryString = 'SELECT '.$select.' FROM `Image` as d WHERE `enabled` = 1 AND id in
			(SELECT image_id FROM (select ac.image_id from Image_Client as ac
			WHERE '.$subWhere.' ac.enabled = 1) as filtered)
			' . $where . '';
		} else {
			$queryString = '
				SELECT '.$select.'
			  	FROM
			  		(SELECT image_id FROM Image_Client WHERE client_id IN ( '.implode(',', $clientIds).' ) GROUP BY image_id HAVING COUNT(DISTINCT client_id) = '.count($clientIds).' ) AS x
				INNER JOIN `Image` as d
				ON d.id = x.image_id
				WHERE d.id > 0 '.$where.'
			';
		}
		$queryString = $queryString." ORDER BY d.id DESC LIMIT $from, $to";
		$query = $db->prepare($queryString);
		$query->execute();
		$items = $query->fetchAll(PDO::FETCH_OBJ);
		$query->closeCursor();
		return $items;
	}


}