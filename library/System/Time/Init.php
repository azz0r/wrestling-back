<?php
/**
 * Created by JetBrains PhpStorm.
 * User: aaronlote
 * Date: 15/01/2013
 * Time: 11:35
 * To change this template use File | Settings | File Templates.
 */
class Model_Time_Init
{
    function __construct($entered) {

        $this->entered = trim($entered);

        if (preg_match("/^\d{4}-\d{2}-\d{2}$/", $this->entered)) {
            $timeArray = explode('-', $this->entered);
            $this->timestamp 	= mktime(0, 1, 1, $timeArray[1], $timeArray[2], $timeArray[0]);

        } else if (preg_match("/^\d{4}-\d{2}-\d{2} [0-2][0-3]:[0-5][0-9]:[0-5][0-9]$/", $this->entered)) {
            $explode			= explode(' ', $this->entered);
            $dateArray 			= explode('-', $explode[0]);
            $timeArray			= explode(':', $explode[1]);
            $this->timestamp 	= mktime($timeArray[0], $timeArray[1], $timeArray[2], $dateArray[1], $dateArray[2], $dateArray[0]);
        } else {
            $this->timestamp 	= strtotime($this->entered);
        }

        $this->datetime = date("Y-m-d H:i:s", $this->timestamp);
        $locations 		= array('2' => 'Europe/London', '1' => 'America/Los_Angeles');

        foreach ($locations as $key => $location) {
            $date = new DateTime($this->datetime, new DateTimeZone($location));
            $this->return[$key]['location'] = $location;
            $this->return[$key]['datetime'] = $date->format('Y-m-d H:i:sP');
            $this->return[$key]['timestamp'] = $this->timestamp;
            if ($key == 2) {
                $dateFormat = 'm-d-Y';
            } else {
                $dateFormat = 'd-m-Y';
            }

            $this->return[$key]['date'] 	= $date->format($dateFormat);
            $this->return[$key]['time_AM'] 	= $date->format('g:i A');
            $this->return[$key]['time_24'] 	= $date->format('G:i');
            $this->return[$key]['timestamp'] = $date->getTimestamp();
        }

        return $this->return;
    }
}
