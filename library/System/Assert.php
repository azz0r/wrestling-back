<?php


/*************************************************************************
 * assert.class.php                                                      *
 *************************************************************************
 *                                                                       *
 * (c) 2008-2012 Wolf Software Limited <support@wolf-software.com>       *
 * All Rights Reserved.                                                  *
 *                                                                       *
 * This program is free software: you can redistribute it and/or modify  *
 * it under the terms of the GNU General Public License as published by  *
 * the Free Software Foundation, either version 3 of the License, or     *
 * (at your option) any later version.                                   *
 *                                                                       *
 * This program is distributed in the hope that it will be useful,       *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 * GNU General Public License for more details.                          *
 *                                                                       *
 * You should have received a copy of the GNU General Public License     *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>. *
 *                                                                       *
 *************************************************************************/

class System_Assert
{


	private $enabled = true;


	private $error_type = E_RECOVERABLE_ERROR;


	private $messages = array();


	public function __construct ($enabled = true, $error_type = E_RECOVERABLE_ERROR) {

		
		$this->enabled = (bool) $enabled;
		
		if (is_int($error_type)) {
			$this->error_type = $error_type;
		}
		$this->messages['is_not_empty'][false] = "Content of given variable is not in empty.";
		$this->messages['is_empty'][true] = "Content of given variable is empty and should be.";
		
		$this->messages['is_array'][false] = "Content of given variable is not in the tested array.";
		$this->messages['is_array'][true] = "Content of given variable is in the tested array but should not be.";
		$this->messages['is_a'][false] = "Object given is not instance of '\$class'.";
		$this->messages['is_a'][true] = "Object given is instance of '\$class' but should not be.";
		$this->messages['is_array'][false] = "Content of given variable is not an array.";
		$this->messages['is_array'][true] = "Content of given variable is an array but should not be.";
		$this->messages['is_bool'][false] = "Content of given variable is not a boolean.";
		$this->messages['is_bool'][true] = "Content of given variable is a boolean but should not be.";
		$this->messages['is_callable'][0][false] = "Name given is not a function.";
		$this->messages['is_callable'][0][true] = "Name given is a function but should not be.";
		$this->messages['is_callable'][1][false] = "Name given is not a method of \$var[0].";
		$this->messages['is_callable'][1][true] = "Name given is a method of \$var[0] but should not be.";
		$this->messages['is_dir'][false] = "Path given is not a directory.";
		$this->messages['is_dir'][true] = "Path given is a directory but should not be.";
		$this->messages['is_double'][false] = "Content of given variable is not a double.";
		$this->messages['is_double'][true] = "Content of given variable is a double but should not be.";
		$this->messages['is_executable'][false] = "Path given is not an executable file.";
		$this->messages['is_executable'][true] = "Path given is an executable file but should not be.";
		$this->messages['is_file'][false] = "Path given is not a file.";
		$this->messages['is_file'][true] = "Path given is a file but should not be.";
		$this->messages['is_finite'][false] = "Content of given variable is not finite.";
		$this->messages['is_finite'][true] = "Content of given variable is finite but should not be.";
		$this->messages['is_float'][false] = "Content of given variable is not a float.";
		$this->messages['is_float'][true] = "Content of given variable is a float but should not be.";
		$this->messages['is_infinite'][false] = "Content of given variable is not infinite.";
		$this->messages['is_infinite'][true] = "Content of given variable is infinite but should not be.";
		$this->messages['is_int'][false] = "Content of given variable is not an integer.";
		$this->messages['is_int'][true] = "Content of given variable is an integer but should not be.";
		$this->messages['is_integer'][false] = "Content of given variable is not an integer.";
		$this->messages['is_integer'][true] = "Content of given variable is an integer but should not be.";
		$this->messages['is_link'][false] = "Path given is not a link.";
		$this->messages['is_link'][true] = "Path given is a link but should not be.";
		$this->messages['is_long'][false] = "Content of given variable is not a long integer.";
		$this->messages['is_long'][true] = "Content of given variable is a long integer but should not be.";
		$this->messages['is_nan'][false] = "Content of given variable is a number.";
		$this->messages['is_nan'][true] = "Content of given variable is not a number but should not be.";
		$this->messages['is_null'][false] = "Content of given variable is not null.";
		$this->messages['is_null'][true] = "Content of given variable is null but should not be.";
		$this->messages['is_numeric'][false] = "Content of given variable is not numeric.";
		$this->messages['is_numeric'][true] = "Content of given variable is numeric but should not be.";
		$this->messages['is_object'][false] = "Content of given variable is not an oject.";
		$this->messages['is_object'][true] = "Content of given variable is an object but should not be.";
		$this->messages['is_readable'][false] = "Path given is not readable.";
		$this->messages['is_readable'][true] = "Path given is readable but should not be.";
		$this->messages['is_real'][false] = "Content of given variable is not a real.";
		$this->messages['is_real'][true] = "Content of given variable is a real but should not be.";
		$this->messages['is_resource'][false] = "Content of given variable is not a resource.";
		$this->messages['is_resource'][true] = "Content of given variable is a resource but should not be.";
		$this->messages['is_scalar'][false] = "Content of given variable is not a scalar.";
		$this->messages['is_scalar'][true] = "Content of given variable is a scalar but should not be.";
		$this->messages['is_string'][false] = "Content of given variable is not a string.";
		$this->messages['is_string'][true] = "Content of given variable is a string but should not be.";
		$this->messages['is_subclass_of'][false] = "Object given is not subclass of '\$class'.";
		$this->messages['is_subclass_of'][true] = "Object given is subclass of '\$class' but should not be.";
		$this->messages['is_writable'][false] = "Path given is not writable.";
		$this->messages['is_writable'][true] = "Path given is writable but should not be.";
		$this->messages['is_writeable'][false] = "Path given is not writable.";
		$this->messages['is_writeable'][true] = "Path given is writable but should not be.";
	}


	public function trigger_error ($message, $file = null, $line = null) {

		if (!is_null($file) && !is_null($line)) {
			$message .= " This error has been triggered from <strong>'{$file}'</strong> at line <strong>{$line}</strong> and called ";
		} else if (!is_null($file)) {
			$message .= " This error has been triggered from <strong>'{$file}'</strong> and called ";
		} else if (!is_null($line)) {
			$message .= " This error has been triggered at line <strong>{$line}</strong> and called ";
		} else {
			$message .= " This error has been called ";
		}

	}


	public function in_array ($needle, $array, $trigger_when = false, $file = null, $line = null) {

		if (($this->enabled) && ($this->is_array($array, false, __FILE__, __LINE__))) {
			if (in_array($needle, $array) === $trigger_when) {
				$this->trigger_error($this->messages[in_array][$trigger_when], $file, $line);
				return $trigger_when;
			}
		}
		return in_array($needle, $array);
	}


	public function is_a ($object, $class, $trigger_when = false, $file = null, $line = null) {

		if ($this->enabled) {
			if (is_a($object, $class) === $trigger_when) {
				$this->trigger_error(eval("return \"" . $this->messages[is_a][$trigger_when] . "\";"), $file, $line);
			}
		}
		return is_a($object, $class);
	}


    public function is_null_or_is_date($date) {
        if (is_null($date) || empty($date)) {
            return true;
        } else {
            return $this->is_date($date);
        }
    }

    public function is_date($date) {

       $date =  explode("-",$date);

       if (count($date) < 2) {
           return false;
       }
       list($yy,$mm,$dd) = $date;

        if (is_numeric($yy) && is_numeric($mm) && is_numeric($dd)) {
            return true;
        } else {
            return false;
        }
    }


	public function is_empty($string) {
		return empty($string);
	}


	public function is_not_empty($string) {
		return !empty($string);
	}


	public function is_not_empty_and_is_numeric($string) {
		$string = (int) $string;
		return ($string > 0) ? true : false;
	}


    public function is_email($string) {
        return filter_var($string, FILTER_VALIDATE_EMAIL);
    }


	public function is_callable ($var, $syntax_only = null, &$callable_name = null, $trigger_when = false, $file = null, $line = null) {

		if ($this->enabled) {
			if (is_null($syntax_only)) {
				if (is_callable($var, false) === $trigger_when) {
					$this->trigger_error($this->messages[is_callable][0][$trigger_when], $file, $line);
				}
			} else {
				if (is_callable($var, $syntax_only) === $trigger_when) {
					$this->trigger_error(eval("return \"" . $this->messages[is_callable][1][$trigger_when] . "\";"), $file, $line);
				}
			}
		}
		return is_callable($var, $syntax_only, $callable_name);
	}


	public function is_subclass_of ($object, $class, $trigger_when = false, $file = null, $line = null) {

		if ($this->enabled && $this->is_object($object, false, __FILE__, __LINE__) && $this->is_string($class, false, __FILE__, __LINE__)) {
			if (is_subclass_of($object, $class) === $trigger_when) {
				$this->trigger_error(eval("return \"" . $this->messages[is_subclass_of][$trigger_when] . "\";"), $file, $line);
			}
		
		}
		return is_subclass_of($object, $class);
	}


	public function __call ($name, $arguments) {

		$allowed = Array(
			'is_empty', 
			'is_array',
            'is_email',
			'is_bool', 
			'is_dir', 
			'is_double', 
			'is_executable', 
			'is_file', 
			'is_finite', 
			'is_float', 
			'is_infinite', 
			'is_int', 
			'is_integer', 
			'is_link', 
			'is_long', 
			'is_nan', 
			'is_null', 
			'is_numeric', 
			'is_object', 
			'is_readable', 
			'is_real', 
			'is_resource', 
			'is_scalar', 
			'is_string', 
			'is_uploaded_file', 
			'is_writable', 
			'is_writeable');
		
		if (!(in_array($name, $allowed))) {
			$this->trigger_error("Fatal error: Call to undefined method $name");
		}
		$var = $arguments[0];
		$trigger_when = (isset($arguments[1])) ? $arguments[1] : false;
		$file = (isset($arguments[2])) ? $arguments[2] : null;
		$line = (isset($arguments[3])) ? $arguments[3] : null;
		
		if ($this->enabled) {
			if ($name($var) === $trigger_when) {
				return false;
				$this->trigger_error($this->messages[$name][$trigger_when], $file, $line);
			}
		}
		return $name($var);
	}
}

?>
