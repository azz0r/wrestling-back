<?php 


/**
 * Master rest request class
 * 
 * This file... 
 * @author Aaron Lote <aaron@theaudience.com>
 * @copyright The Audience
 * @version 2.0
 * @package master
 */


class System_Rest_Request {


	private $request_vars;
	private $data;
	private $http_accept;
	private $method;

	private $output_data;
	private $output_response;


	public function __construct() {

		$this->request_vars		= array();
		$this->data				= NULL;
		$_GET['http_accept'] 	= isset($_GET['return']) ? $_GET['return'] : 'json';

		if (isset($_SERVER['HTTP_ACCEPT'])) {
			$this->http_accept = (strpos($_SERVER['HTTP_ACCEPT'], 'json')) ? 'json' : $_GET['http_accept'];
		}

		$this->method = 'get';
	}

	
	public function getParams($array) {
		return array_intersect_key($this->request_vars[$this->method], array_flip($array));
	}


	public function setData($data) {
		$this->data = $data;
	}


	public function setErrorData($id, $data=array()) {

		$errors = System_Errors::get();

		$error 	= isset($errors[(int) $id]) && isset($errors[(int) $id]['message']) ? $errors[$id]['message'] : $id.': Error Code Is Currently Not Recorded';
		$status = isset($errors[(int) $id]) && isset($errors[(int) $id]['header']) ? $errors[$id]['header'] : 500;
		$return['errors'] = array('code' => $id, 'message' => $error, 'data' => $data);

		$this->setOutputResponse($status);
		$this->setOutputData($return);
	}


	/**
    * sets the method for this request
    * @param string put|delete|post
    */
	public function setMethod($method) {
		$this->method = $method;
	}


	/**
    * sets the request vars for this request
    * @param array 
    */
	public function setRequestVars($request_vars) {
		$this->request_vars = $request_vars;
	}


	public function _getParam($param, $fallback=NULL) {
		if (isset($this->request_vars[$this->getMethod()][$param])) {
			return $this->request_vars[$this->getMethod()][$param];
		} else {
			/*Example of why this was introduced
			 * Im in a PUT request and I'm doing $this->_getParam
			 * which would look for a PUT _id, however its in the URL for _id
			 * So we loop through them, then if it exists return that
			 */
			$types = array('get','put','post','delete');
			
			foreach ($types as $type) {
				if (isset($this->request_vars[$type][$param])) {
					return $this->request_vars[$type][$param];
				}
			}
			return $fallback;
		}
	}


	/**
    * gets the request vars for this request
    * @return array
    */
	public function getRequestVars($type=NULL) {
		if (is_null($type)) {
			return $this->request_vars;
		} else {
			if (isset($this->request_vars[$type])) {
				return $this->request_vars[$type];
			} else {
				return array();
			}
		}
	}


	//output data - aaron
	public function setOutputData($data) {
		$this->output_data = $data;
	}


	public function getOutputData() {
		return $this->output($this->output_data);
	}


	public function getRawData() {
		return $this->output_data;
	}


	//response output - aaron
	public function setOutputResponse($output_response) {
		$this->output_response = $output_response;
	}


	public function getOutputResponse() {
		return $this->output_response;
	}


	//figure ut what were sending and use correct class - aaron
    private function output($data=array()){

        $data = empty($data) ? array() : $data;
        switch ($this->getHttpAccept()) {
            case 'json' : default:
            return System_Rest_Output_JSON::valid($data);
            break;
            case 'xml' :
                return System_Rest_Output_XML::valid($data);
            break;
            case 'html' :
                return System_Rest_Output_HTML::valid($data);
            break;

        }
    }


	public function sendResponse() {
		return System_Rest::sendResponse($this->getOutputResponse(), $this->getOutputData(), $this->getHttpAccept(1));
	}


	public function get_PUT($id, $default_to=NULL) {

		$vars = $this->getRequestVars();

		if (isset($vars['put'][$id])) {
			return $vars['put'][$id];
		} else {
			return $default_to;
		}
	}


	public function get_POST($id, $default_to=NULL) {

		$vars = $this->getRequestVars();
		if (isset($vars['post'][$id])) {
			return $vars['post'][$id];
		} else {
			return $default_to;
		}
	}


	public function get_DELETE($id, $default_to=NULL) {

		$vars = $this->getRequestVars();
		if (isset($vars['delete'][$id])) {
			return $vars['delete'][$id];
		} else if (isset($vars['post'][$id])) {
			return $vars['post'][$id];
		}
	}


	public function setParam($type, $key, $value) {
		$this->request_vars[$type][$key] = $value;
	}
	public function setMainParam($type, $value) {
		$this->request_vars[$type] = $value;
	}
	public function clearParams() {
		$this->request_vars = NULL;
		$this->setData(array());
	}


	public function getMethod() {
		return $this->method;
	}


	public function getHttpAccept($mime=0) {
		if ($mime == 1) {
			if ($this->http_accept == 'json') {
				return 'application/json';
			} else if ($this->http_accept == 'xml') {
				return 'application/xml';
			} else if ($this->http_accept == 'html') {
				return 'text/html';
			} else {
				return $this->http_accept;
			}
		} else {
			return $this->http_accept;
		}
	}


}