<?php 


class System_Rest_Output_XML {


	public static function xml_from_array($array, $node_name) {
		$xml = NULL;
	
		if (is_array($array) || is_object($array)) {
			foreach ($array as $key=>$value) {
				if (is_numeric($key)) {
					$key = $node_name;
				}
	
				$xml .= '<' . $key . '>' . "\n" . self::xml_from_array($value, $node_name) . '</' . $key . '>' . "\n";
			}
			return $xml;
		} else {
			return htmlspecialchars($array, ENT_QUOTES) . "\n";
		}
	}


	public static function valid($array, $node_block='nodes', $node_name='node') {

		$xml = '<?xml version="1.0" encoding="UTF-8" ?>' . "\n";
		$xml .= '<' . $node_block . '>' . "\n";
		$xml .= self::xml_from_array($array, $node_name);
		$xml .= '</' . $node_block . '>' . "\n";
		return $xml;
	}
	
}