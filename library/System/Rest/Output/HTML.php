<?php 


class System_Rest_Output_HTML {


	public static function valid($data) {
		return '
			<html>
				<header>
					<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
					<title>API Output</title>
					<style>
						body {
							background: black;
							color: white;
							line-height: 2em;
							font-size: 13px;
						}
					</style>
				</header>
				<body>
					<pre>
						'.print_r($data, true).'
					</pre>
				</body>
			</html>
		';
	}
	
}