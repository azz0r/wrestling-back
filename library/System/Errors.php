<?php


/**
 *
 * @author Aaron Lote <aaron@theaudience.com>
 * @copyright The Audience
 * @version 2.0
 * @package master
 */

class System_Errors
{


	public static function get () {

		$errors = array();
		$errors['1'] = array(
			'header' => 500, 
			'message' => 'Please retrieve data by using the URL schema: object.json / object/id.json.');
		$errors['2'] = array(
			'header' => 500, 
			'message' => 'This method is deprecated');
		$errors['410'] = array(
			'header' => 410, 
			'message' => 'This object has gone (deleted)');
		// post
		$errors['12'] = $errors['404'] = array(
			'header' => 404, 
			'message' => 'The object you requested doesnt exist');
		$errors['13'] = array(
			'header' => 401, 
			'message' => 'You do not have permission to edit this object');
		$errors['14'] = array(
			'header' => 401, 
			'message' => 'You do not have permission to add this object');
		$errors['15'] = array(
			'header' => 500, 
			'message' => 'Access token for API was not found'); // API_Key
		$errors['16'] = array(
			'header' => 500, 
			'message' => 'Data was incorrectly formatted');
		$errors['18'] = array(
			'header' => 501, 
			'message' => 'The analytics engine response was incorrectly formatted');

		$errors['250'] = array(
			'header' => 409, 
			'message' => 'An object exists with the same name');
		$errors['251'] = array(
			'header' => 500, 
			'message' => 'We couldnt delete the object you requested');
		$errors['252'] = array(
			'header' => 404, 
			'message' => 'We couldnt find the object you requested');

		
		// account/index : put
		$errors['126'] = array(
			'header' => 500, 
			'message' => 'id is null');
		$errors['127'] = array(
			'header' => 500, 
			'message' => 'account object is empty');
		
		$errors['178'] = $errors['734'] = $errors['724'] = $errors['744'] = $errors['126'] = $errors['996'] = array(
			'header' => 500, 
			'message' => 'ID is NULL.');
		$errors['179'] = array(
			'header' => 500, 
			'message' => 'account is empty');
		
		// client/get
		$errors['459'] = array(
			'header' => 404, 
			'message' => 'Client requested could not be retrieved');
		$errors['454'] = array(
			'header' => 401, 
			'message' => 'You do not have permission to fetch this client');
		
		// content/get
		$errors['450'] = array(
			'header' => 500, 
			'message' => 'Content requested could not be retrieved');
		$errors['456'] = array(
			'header' => 401, 
			'message' => 'You do not have permission to fetch this clients content');
		$errors['554'] = array(
			'header' => 500, 
			'message' => 'There are no clients bound to your account, therefore we have no content for you');
		
		// content/post
		$errors['612'] = array(
			'header' => 404, 
			'message' => 'No client found');
		$errors['656'] = array(
			'header' => 401, 
			'message' => 'You do not have permission to send content to this client');
		$errors['632'] = array(
			'header' => 500, 
			'message' => 'Unable to process image');
		
		// content-image delete
		// $errors['724'] = array('header' => 500, 'message' => 'id is null');
		$errors['726'] = array(
			'header' => 404, 
			'message' => 'could not load the image requested');
		$errors['728'] = array(
			'header' => 401, 
			'message' => 'you do not have access to this client');
		$errors['730'] = array(
			'header' => 500, 
			'message' => 'we couldnt delete that image');
		
		// asset-item add
		$errors['635'] = array(
			'header' => 500, 
			'message' => 'Unable to verify you as the original asset poster');
		$errors['636'] = array(
			'header' => 501, 
			'message' => 'Media type unsupported');
		$errors['637'] = array(
			'header' => 501, 
			'message' => 'Could not find asset');
		$errors['501'] = array(
			'header' => 501, 
			'message' => 'General error, see attached message');
		
		// content-link delete
		// $errors['734'] = array('header' => 500, 'message' => 'id is null');
		$errors['736'] = array(
			'header' => 404, 
			'message' => 'could not load the link requested');
		$errors['738'] = array(
			'header' => 401, 
			'message' => 'you do not have access to this client');
		$errors['740'] = array(
			'header' => 500, 
			'message' => 'we couldnt delete the link');
		
		// content-text delete
		// $errors['744'] = array('header' => 500, 'message' => 'id is null');
		$errors['746'] = array(
			'header' => 404, 
			'message' => 'could not load the link requested');
		$errors['748'] = array(
			'header' => 401, 
			'message' => 'you do not have access to this client');
		$errors['750'] = array(
			'header' => 500, 
			'message' => 'we couldnt delete the link');
		
		// content put
		$errors['877'] = array(
			'header' => 404, 
			'message' => 'the content you requested to update could not be loaded');
		$errors['879'] = array(
			'header' => 401, 
			'message' => 'you do not have access to this clients content');
		$errors['880'] = array(
			'header' => 500, 
			'message' => 'we couldnt save the content you tried to save');
		
		// invite put and get
		$errors['922'] = array(
			'header' => 500, 
			'message' => 'System error, image could not be created');
		
		// master password
		$errors['918'] = array(
			'header' => 500, 
			'message' => 'Password may not be null');
		$errors['919'] = array(
			'header' => 500, 
			'message' => '');
		
		// client_manager fetch
		$errors['760'] = array(
			'header' => 500, 
			'message' => 'Client or user id required to filter results');
		$errors['761'] = array(
			'header' => 401, 
			'message' => 'Permission check failed for this client to your user');
		$errors['762'] = array(
			'header' => 500, 
			'message' => 'You do not have permission to the client');
		$errors['763'] = array(
			'header' => 500, 
			'message' => 'Could not load Client Manager row to update');
		$errors['764'] = array(
			'header' => 404, 
			'message' => 'Both client id and user must be present');
		
		// user add
		$errors['800'] = array(
			'header' => 500, 
			'message' => 'Required fields were not filled in');
		$errors['801'] = array(
			'header' => 500, 
			'message' => 'A user with that user email already exists');
		
		// user edit
		$errors['804'] = array(
			'header' => 500, 
			'message' => 'Could not load the user requested');
		$errors['805'] = array(
			'header' => 500, 
			'message' => 'A user with that user email already exists');
		$errors['808'] = array(
			'header' => 500, 
			'message' => 'We couldnt save the user');
		
		// user index
		$errors['806'] = array(
			'header' => 401, 
			'message' => 'You do not have permission to fetch this client');
		$errors['807'] = array(
			'header' => 401, 
			'message' => 'You do not have permission to fetch a user list');
		$errors['809'] = array(
			'header' => 404, 
			'message' => 'User requested was not found');
		
		// authentication
		$errors['666'] = array(
			'header' => 401, 
			'message' => 'We could not get the area you requested');
		$errors['777'] = array(
			'header' => 401, 
			'message' => 'Method requested does not yet exist');
		$errors['900'] = array(
			'header' => 401, 
			'message' => 'Authenticate was canceled by app');
		$errors['902'] = array(
			'header' => 401, 
			'message' => 'Could not auth with details passed');
		$errors['903'] = array(
			'header' => 401, 
			'message' => 'Wrong Credentials');
		$errors['904'] = array(
			'header' => 401, 
			'message' => 'Wrong Credentials');
		
		// majors
		$errors['905'] = array(
			'header' => 500, 
			'message' => 'We couldnt connect to the database with details passed');
		$errors['906'] = array(
			'header' => 500, 
			'message' => 'Internal API Routing failed');
		$errors['907'] = array(
			'header' => 500, 
			'message' => 'The requested method does not exist for that controller');
		$errors['908'] = array(
			'header' => 500, 
			'message' => 'Logging could not be started');
		
		// client add
		$errors['600'] = array(
			'header' => 400, 
			'message' => 'Required fields were not filled in');
		$errors['601'] = array(
			'header' => 400, 
			'message' => 'An account with that user email already exists');
		$errors['602'] = array(
			'header' => 400, 
			'message' => 'Email address entered was not valid');
		
		// permission
		$errors['999'] = array(
			'header' => 404, 
			'message' => 'Permission requested does not exist');
		$errors['998'] = array(
			'header' => 401, 
			'message' => 'You do not have the required permission to fetch this users permissions');
		$errors['997'] = array(
			'header' => 401, 
			'message' => 'To create a users permissions we require a group id');
		// $errors['996'] = array('header' => 500, 'message' => 'id is null');
		$errors['995'] = array(
			'header' => 500, 
			'message' => 'You don\'t have require permission to perform this action');
		
		// client put
		$errors['606'] = array(
			'header' => 401, 
			'message' => 'We couldnt load the client id you requested');
		$errors['607'] = array(
			'header' => 401, 
			'message' => 'You do not have the user permission required to make this action');
		$errors['608'] = array(
			'header' => 401, 
			'message' => 'Email address must match something@theaud.co');
		$errors['945'] = array(
			'header' => 401, 
			'message' => 'Email exists');
		$errors['944'] = array(
			'header' => 401, 
			'message' => 'You must be an administrator to perform this action');
		
		$errors['511'] = array(
			'header' => 500, 
			'message' => 'Both password reminder token and account id must be sent');
		$errors['512'] = array(
			'header' => 401, 
			'message' => 'The token id we recieved does not match the token id present');
		$errors['513'] = array(
			'header' => 500, 
			'message' => 'We were unable to save your new password, please try again');
		$errors['514'] = array(
			'header' => 500, 
			'message' => 'Password and Confirmation Password must be the same');
		
		// facebook post
		$errors['212'] = array(
			'header' => 404, 
			'message' => 'Facebook Post requested could not be retrieved');
		// facebook place
		$errors['213'] = array(
			'header' => 404, 
			'message' => 'Facebook Places requested could not be retrieved');
		// facebook metrics
		$errors['214'] = array(
			'header' => 404, 
			'message' => 'Facebook Metrics requested could not be retrieved');
		
		// facebook get
		$errors['250'] = array(
			'header' => 404, 
			'message' => 'Please set all required fields (_id, client_id)');
		$errors['251'] = array(
			'header' => 404, 
			'message' => 'There was no client facebook details found');
		$errors['252'] = array(
			'header' => 404, 
			'message' => 'Please ensure client has application id and application secret setup correctly');
		$errors['253'] = array(
			'header' => 404, 
			'message' => 'The facebook access token has not been found');
		$errors['254'] = array(
			'header' => 404, 
			'message' => 'Please set the facebook object type');
		$errors['255'] = array(
			'header' => 404, 
			'message' => 'There is a problem with the facebook api requested url path');
		$errors['256'] = array(
			'header' => 404, 
			'message' => 'For the delete method please set _method and _id correctly');
		$errors['257'] = array(
			'header' => 404, 
			'message' => 'There was no Post vars found');
		
		// facebook post
		$errors['260'] = array(
			'header' => 404, 
			'message' => 'Please set the post id correctly');
		
		// twitter
		$error['270'] = array(
			'header' => 404, 
			'message' => 'There was a problem posting to twitter');
		
		// cron
		$errors['458'] = array(
			'header' => 401, 
			'message' => 'They key provided was incorrect');
		// tos put
		$errors['330'] = array(
			'header' => 404, 
			'message' => 'We couldnt load the TOS id you requested');
		$errors['350'] = array(
			'header' => 409, 
			'message' => 'An object exists with the same name');
		

		// external api
		$errors['1000'] = array(
			'header' => 401, 
			'message' => 'table guildlines have not been set');
		$errors['1001'] = array(
			'header' => 401, 
			'message' => 'client id was not set');
		$errors['1002'] = array(
			'header' => 401, 
			'message' => 'client keys are not set');
		$errors['1003'] = array(
			'header' => 401, 
			'message' => 'the sdk has not been init correctly');
		$errors['1004'] = array(
			'header' => 401, 
			'message' => 'one or more of the sdk credentials has not been set');
		$errors['1005'] = array(
			'header' => 401, 
			'message' => 'the external api response was not 200 ok');
		$errors['1006'] = array(
			'header' => 401, 
			'message' => 'there was a problem getting the response');
		$errors['1007'] = array(
			'header' => 401, 
			'message' => 'post id is not set');
		$errors['1008'] = array(
			'header' => 401, 
			'message' => 'there was no post found by the selected post id');
		$errors['1009'] = array(
			'header' => 401, 
			'message' => 'the post type has not been set');
		$errors['1010'] = array(
			'header' => 401, 
			'message' => 'there have been no clients list found for this post');
		$errors['1011'] = array(
			'header' => 401, 
			'message' => 'app id has not been set');
		$errors['1012'] = array(
			'header' => 401, 
			'message' => 'app secret has not been set');
		$errors['1013'] = array(
			'header' => 401, 
			'message' => 'app access token has not been set');
		$errors['1014'] = array(
			'header' => 401, 
			'message' => 'page access token has not been set');
		$errors['1015'] = array(
			'header' => 401, 
			'message' => 'object type has not been set');
		$errors['1016'] = array(
			'header' => 401, 
			'message' => 'object id has not been set');
		$errors['1017'] = array(
			'header' => 401, 
			'message' => 'post text has not been set');
		$errors['1018'] = array(
			'header' => 401, 
			'message' => 'post link has not been set');
		$errors['1019'] = array(
			'header' => 401, 
			'message' => 'post name has not been set');
		$errors['1020'] = array(
			'header' => 401, 
			'message' => 'No Albums have been found on the facebook page');
		$errors['1021'] = array(
			'header' => 401, 
			'message' => 'There has been no wall album found on the facebook page');
		$errors['1022'] = array(
			'header' => 401, 
			'message' => 'image path has not been found');
		$errors['1023'] = array(
			'header' => 401, 
			'message' => 'image filename has not been found');
		$errors['1024'] = array(
			'header' => 401, 
			'message' => 'post source has not been set');
		$errors['1025'] = array(
			'header' => 401, 
			'message' => 'there was no records found within Post_Facebook');
		$errors['1026'] = array(
			'header' => 401, 
			'message' => 'there was no images found to post to gallery');
		$errors['1027'] = array(
			'header' => 401, 
			'message' => 'This Gallery publishing feature is currently disabled');
		

		/* Audalytics/Page/Overview */
		$errors['5100'] = array(
			'header' => 404, 
			'message' => 'The facebook page id has not been entered');
		$errors['5101'] = array(
			'header' => 404, 
			'message' => 'No metrics were receieved from the Audalytics engine');
		$errors['5102'] = array(
			'header' => 401, 
			'message' => 'Problem creating date range for Audalytics request');
		
		$errors['6001'] = array(
			'header' => 500, 
			'message' => 'Facebook failed to publish this post');
		
		$errors['9901'] = array(
			'header' => 500, 
			'message' => 'Twitter Failed to Publish');
		
		
		/* Mongo Controllers */
		$errors['3100'] = array(
				'header' => 400,
				'message' => 'Validation Errors, attached');
		$errors['3101'] = array(
				'header' => 400,
				'message' => 'Cannot be empty');
        $errors['3102'] = array(
            'header' => 400,
            'message' => 'Not a valid email address');
        $errors['3103'] = array(
        		'header' => 400,
        		'message' => 'Id is required');
        $errors['3104'] = array(
            'header' => 400,
            'message' => 'Error when creating the document to save');
        $errors['3105'] = array(
            'header' => 400,
            'message' => 'Publish target is wrong for post type set');
		return $errors;
	}


}