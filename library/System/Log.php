<?php 


class System_Log {


	protected $_config;


	public function __construct() {
		$this->createLogger('logger', 	'debug.log');
		$this->createLogger('queries', 	'queries.log');
		$this->createLogger('warnings', 'warnings.log');
	}

 
	public function createLogger($name, $path) {

		try {
			Zend_Registry::set($name, array());
			$loggerWriter = new Zend_Log_Writer_Stream(_LOG.'/'.$path);
			if ($name == 'queries') {
				$logFormatter = new Zend_Log_Formatter_Simple('%message%' . PHP_EOL);
			} else {
				$logFormatter = new Zend_Log_Formatter_Simple('%timestamp% '.PHP_EOL.'    %message%' . PHP_EOL);
			}
			$loggerWriter->setFormatter($logFormatter);
			$logger = new Zend_Log($loggerWriter);
		    $config = Zend_Registry::get('config');
            $filterPriority = isset($config->logging->priority) ? constant($config->logging->priority) : Zend_Log::ERR;

			$filter = new Zend_Log_Filter_Priority($filterPriority);
			$logger->addFilter($filter);
			Zend_Registry::set($name, $logger);
		} catch (Zend_Log_Exception $e) {
			throw new System_Exception_Application(908);
		}
	}


}