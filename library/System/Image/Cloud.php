<?php 

/**

Assetcloud upload. See http://jenkins.theaudience.com:9000/job/cloud_storage-dev/doclinks/1/
for details.

*/

class System_Image_Cloud {

    /*
        Generate array with variant information, so you can pass it
	as variant_info to System_Image_Cloud::upload.

        Params:
            $original_path: path to original asset
            $name: name of the variant (ie. small)
            $v_class: class of variant.
    */
    public static function variant_info($original_path, $name, $v_class='img_resize' ) {

        return array(
            'variant_of' => $original_path,
            'name' => $name,
            'class' => $v_class
        );
    }


	public static function upload($data, $variant_info = NULL) {

        $asset_data = array('path' => $data['path']);

        if (!is_null($variant_info)) {
            $asset_data['variant_info'] = $variant_info;
        }

		$fields = array(
				'access_key' 	=> ASSET_CLOUD_UPLOAD_ACCESS_KEY,
				'data' 			=> json_encode($asset_data),
				'file'			=> "@".$data['file'],
		);

		//open connection
		$ch = curl_init();

		//set the url, number of POST vars, POST data
		curl_setopt($ch,	CURLOPT_URL, 				ASSET_CLOUD_UPLOAD_URL);
		curl_setopt($ch,	CURLOPT_TIMEOUT, 			ASSET_CLOUD_UPLOAD_TIMEOUT);

		curl_setopt($ch, 	CURLOPT_POST, 				TRUE);
		curl_setopt($ch, 	CURLINFO_HEADER_OUT, 		TRUE);
		curl_setopt($ch, 	CURLOPT_RETURNTRANSFER, 	TRUE);
		curl_setopt($ch,	CURLOPT_POSTFIELDS, 		$fields);

		//execute post
		$result = curl_exec($ch);

        $curl_errno = curl_errno($ch);
        $curl_error = curl_error($ch);

		//close connection
		curl_close($ch);

        if ($curl_errno > 0) {
            throw new Exception('Cloud upload - curl error: ' . $curl_errno . ' ' . $curl_error);
        }


		return $result;
	}


    /*
        Upload set of images to cloud.
        Params:
            $imagePaths: array($name => $pathObject),
            where $name is image variant name (original, assetThumb, facebook, etc.),
            and $pathObject is an array('file'=> path to file on disk, 'path' => desired path on cloud server
    */

    public static function upload_images($imagePaths) {

        #upload original first, then all remaining variants
        $cloudResponse = System_Image_Cloud::upload($imagePaths['original']);
        $cloudResponse = json_decode($cloudResponse, true);
        if( $cloudResponse['status'] != 'ok') {
            throw new Exception('cloud upload error: '. $imagePaths['original']['path']);
        }

        foreach ($imagePaths as $key => $path) {
            if ($key == 'original')
                continue;

            $variant_info = System_Image_Cloud::variant_info($imagePaths['original']['path'], $key);
            $cloudResponse = System_Image_Cloud::upload($path, $variant_info);
            $cloudResponse = json_decode($cloudResponse, true);
            if( $cloudResponse['status'] != 'ok') {
                throw new Exception('cloud upload error: '. $imagePaths['original']['path']);
            }

        }


    }

}
