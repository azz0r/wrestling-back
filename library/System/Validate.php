<?php


class System_Validate {


	public static function email($email) {
		$validator = new Zend_Validate_EmailAddress();
		return $validator->isValid($email);
	}


	public static function isValidUsername($username) {
		
		if (preg_match('/^[a-z\d_]{4,28}$/i', $username)) {
			return true;
		} else {
			return false;
		}
	}


	public static function isValidUserTitle($usertitle) {
		if (self::isValidLength(0,45,$usertitle) == true) {
			return true;
		} else {
			return false;
			#throw new Exception_Application('The entered user title is not valid');
		}
	}


	public static function isValidSignature($signature) {
		if (self::isValidLength(0,400,$signature) == true) {
			return true;
		} else {
			return false;
			#throw new Exception_Application('The entered user title is not valid');
		}
	}


	public static function password($password) {

		if (preg_match('^(?=.*\d).{5,12}$^',$password)) {
			return true;
		} else {
			return false;
		}
	}


	public static function isValidActivationCode($code) {

		if (eregi('^[[:alnum:]]{8}$',$code)) {
			return true;
		} else {
			return false;
		}
	}


	public static function length($min,$max,$string) {

		$length = strlen($string);

		if ($length < $min || $length > $max) {
			return false;
		} else {
			return true;
		}
	}
	
	public static function requiredValues($required = array(), $input = array()) {
		
		foreach($required as $r) {
			if(!array_key_exists($r, $input) || empty($input[$r])) {
				return false;
			}
		}
		return true;
	}


}	


?>