<?php


/* This should be in System @todo */
class System_Database_Mongo {

    private $config;


    public static function getConnection() {

        /* we are the website */
        if (defined('BACKSTAGE') || defined('STAGE')) {
            $config = new Zend_Config_Ini(_INI_PATH_,'default');
            $dbParams = array(
                "host" => $config->resources->db->params->host,
                "username" => $config->resources->db->params->username,
                "password" => $config->resources->db->params->password,
                "dbname" => $config->resources->db->params->dbname
            );
            $mongoDB = new Mongo("mongodb://{$dbParams['host']}");
            $database = $mongoDB->{$dbParams['dbname']};  // change the name to a variable
            /* we are the API */
        } else {
            $config = new Zend_Config_Ini(INI_PATH);
            $mongoDB = new Mongo("mongodb://{$config->db_host}");
            $database =  $mongoDB->{$config->db_table};
        }

        Zend_Registry::set('db', $database);
        return $database;
    }

}