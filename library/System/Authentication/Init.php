<?php


class System_Authentication_Init {


    const REALM = 'theaudienceauth';

    protected $user_permissions = array();
    protected $user;
    protected $mongoDB;

    public static $instance;


    public function __construct() {
        $this->mongoDB = System_Database_Mongo::getConnection();
    }


    public static function getInstance() {

        if (!isset(self::$instance)) {
            self::$instance = new self();
        }
        return self::$instance;
    }


    protected function extractData() {

        $realm = self::REALM;

        if (empty ( $_SERVER ['PHP_AUTH_DIGEST'] )) {
            header ( 'HTTP/1.1 401 Unauthorized' );
            header ( 'WWW-Authenticate: Digest realm="' . $realm . '",qop="auth",nonce="' . uniqid () . '",opaque="' . md5 ( $realm ) . '"' );
            exit ();
            die ( 'Text to send if user hits Cancel button' );
        }

        //be careful username on account isnt the same as username in the db also, later
        $data = http_digest_parse ( $_SERVER ['PHP_AUTH_DIGEST'] );
        return $data;
    }


    protected function validateData($data, $password) {

        $realm 			= self::REALM;
        $A1 			= md5 ( $data ['username'] . ':' . $realm . ':' . $password );
        $A2 			= md5 ( $_SERVER ['REQUEST_METHOD'] . ':' . $data ['uri'] );
        $valid_response = md5 ( $A1 . ':' . $data ['nonce'] . ':' . $data ['nc'] . ':' . $data ['cnonce'] . ':' . $data ['qop'] . ':' . $A2 );

        if ($data ['response'] != $valid_response) {
            session_destroy();
            session_unset();
            session_regenerate_id();
            die ( 'Wrong Credentials!' );
        }
    }


    public function authData($data) {

        if (isset ($data ['username'])) {
            $user = (object) $this->mongoDB->User->findOne(array('email' => $data['username'], 'enabled' => true, 'deleted' => false));

            if (is_object($user) && isset($user->encrypted_password)) {
                $encryptedPassword = decrypt_password ($user->encrypted_password );

                if (! defined ( 'CONNECTED_ACCOUNT_ID' )) {
                    define ('CONNECTED_ACCOUNT_ID', $user->id);
                }

                $_SESSION ['logged_in'] 			= true;
                $_SESSION ['CONNECTED_ACCOUNT_ID'] 	= $user->id;
                $_SESSION ['email'] 				= $data['username'];
                $user 							= NULL;
            } else {
                die ( 'Could not auth with details passed: email: ' . $data ['username'] );
            }
        }

        return array($encryptedPassword);
    }


    /**
     * Main auth method
     * @throws System_Exception_Application
     */
    public function authenticate() {

        if (! isset ( $_SESSION ['logged_in'] ) || $_SESSION ['logged_in'] !== true) {

            $data = $this->extractData();
            list($passwordReturned) = $this->authData($data);

            // analyze the PHP_AUTH_DIGEST variable
            if (! ($data)) {
                throw new System_Exception_Application ( 903 );
                die ( 'Wrong Credentials!' );
            }

            $this->validateData($data, $passwordReturned);

        } else {
            if (! defined ( 'CONNECTED_ACCOUNT_ID' )) {
                define ( 'CONNECTED_ACCOUNT_ID', $_SESSION ['CONNECTED_ACCOUNT_ID'] );
            }
        }

        $this->user = $_SESSION['user'] = (object) $this->mongoDB->User->findOne(array('id' => CONNECTED_ACCOUNT_ID));
    }


    public function isAdmin() {
        return false;
    }


    /**
     * returns user id
     * @return User id
     */
    public function getUserId() {
        return $this->user->id;
    }


    /* Added by aaron
     * returns the internal user object
     * */
    public function getUserObject() {
        Zend_Registry::set('user', $this->user);
        return $this->user;
    }



}
