<?php 


class System_Authentication_Session {


	public function __construct() {
				
		//trigger session
		if (isset($_GET['session_id']) ){
			session_id($_GET['session_id']);
		} else {
			session_id();
			$session_id = session_id();
		}
		session_set_cookie_params(86400);
		//@session_start();
		Zend_Session::start();
		self::cleanDebugOutput();
		
	}
	
	public static function cleanDebugOutput(){
		$_SESSION['temp'] = null;
		$_SESSION['temp']['logged_messages'] 	= array();
		$_SESSION['temp']['cache'] 				= array();
		$_SESSION['temp']['cache']['outputs'] 	= null;
		$_SESSION['temp']['cache']['writes'] 	= null;
	}


}