<?php

class System_Authentication_Session_Data
{
    protected $_name = 'Session';

    private $_config;

    private $_db;

    public function __construct() {
        $this->getConnection();
    }

    private function getConnection() {
        $this->_config = new Zend_Config_Ini(INI_PATH);
        $dbParams = array();
        $dbParams['db_host'] = $this->_config->db_host;
        $dbParams['db_user'] = $this->_config->db_user;
        $dbParams['db_pass'] = $this->_config->db_pass;
        $dbParams['db_table'] = $this->_config->db_table;
        $mongoDB = new Mongo("mongodb://{$dbParams['db_host']}");
        $this->_db = $mongoDB->{$dbParams['db_table']};

        return $this->_db->{$this->_name};
    }

    public function find($id) {
        $session = $this->_db->{$this->_name}->findOne(array('_id' => $id));
        return is_null($session) ? array() : (object) $session;
    }

    public function insert($id){
        $this->_db->{$this->_name}->insert(array('_id' => $id, 'session_data' => 0, 'created' => time(), 'updated' => 0));
        return true;
    }

    public function update($id, $sessionData){
        $this->_db->{$this->_name}->update(array('_id' => $id), array('$set' => array('session_data' => $sessionData)));
        return true;
    }

    public function delete($id){
        $this->_db->{$this->_name}->remove(array('_id' => $id));
        return true;
    }

    public function gc($maxLifeTime){
        if(!is_null($maxLifeTime)) $this->_db->{$this->_name}->remove(array('updated' => array('$lt' => $maxLifeTime)));
        return true;
    }



}