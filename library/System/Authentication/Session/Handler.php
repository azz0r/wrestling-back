<?php

class System_Authentication_Session_Handler implements Zend_Session_SaveHandler_Interface
{

	/**
	 * This is instance of My_Session_data, which extends Zend_Db_Table and manages the database connection
	 *
	 * @var My_Session_Data
	 */
	private static $sessionData;

	private static $thisIsOldSession = false;

	private static $originalSessionId = '';


	public function open ($save_path, $name) {
        self::$sessionData = new System_Authentication_Session_Data();
		return true;
	}


	public function close () {

		return true;
	}


    public function read ($id) {
        $row = self::$sessionData->find($id);
        if (!empty($row)) {
            self::$thisIsOldSession = true;
            self::$originalSessionId = $id;
            print_r($row->session_data);die();
            return $row->session_data;
        } else {
            return '';
        }
    }


    public function write ($id, $sessionData) {

        if (self::$thisIsOldSession && self::$originalSessionId != $id) {
            // session ID is regenerated, so set $thisIsOldSession to false, so we insert new row
            self::$thisIsOldSession = false;
        }

        if (self::$thisIsOldSession) {
            self::$sessionData->update($id, $sessionData);
        } else {
            // no such session, create new one
            self::$sessionData->insert($id);
        }

        return true;
    }


    public function destroy ($id) {

        self::$sessionData->delete($id);
        return true;
    }


    public function gc ($maxLifetime) {

        $maxLifetime = (int) $maxLifetime;
        self::$sessionData->gc($maxLifetime);
        return true;
    }


}