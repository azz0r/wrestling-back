<?php


class System_Notification
{


	public function __construct ($options = array()) {

		try {
			$required = array('controller', 'action', 'from', 'data', 'user');
			$this->db = System_Database_Mongo::getConnection();
			$this->controller = (isset($options['controller']) && !empty($options['controller'])) ? $options['controller'] : '';
			$this->action = (isset($options['action']) && !empty($options['action'])) ? $options['action'] : '';
			$this->from = (isset($options['from']) && !empty($options['from'])) ? $options['from'] : '';
			$this->data = (isset($options['data']) && !empty($options['data'])) ? $options['data'] : array();
			$this->options = $options;
			$this->results = array();
			$this->return = array();
			
			// user added for example doesnt have a Notification row (and it shouldnt) so we have an override
			$this->require_row = isset($options['require_row']) ? $options['require_row'] : true;
		} catch (Exception $e) {
			// do something?
		}
		
		$this->match_controller_action();
		$this->load_method();
	}


	public function match_controller_action () {

		if ($this->require_row) {
			$this->notification = (object) $this->db->Notification->findOne(
				array(
					'controller' => $this->controller, 
					'action' => $this->action
				)
			);

			if (empty($this->notification)) {
				throw new Exception('notification could not be loaded');
			} else {
				return true;
			}
		}
	}


	public function load_method () {

		$action = str_replace("-", "_", $this->action);
		$this->{"" . $this->controller . "_" . $action . ""}();
	}


	public function user_added () {

		$this->email_to = $this->from;
		$this->subject = 'Your Invite To theAudience!';
		$this->filename = 'user/added';
		$this->body();
		$this->notifications[] = $this->body;
		$this->send();
		return true;
	}


	public function stage_check () {
	
		$this->email_to = $this->from;
		$this->subject = 'Please Check theAudience Stage';
		$this->filename = 'stage/check';
		$this->body();
		$this->send();
		return true;
	}


	public function __call ($name, $arg) {

		return false;
	}


	public function body () {
		
		// get template
		$view = new Zend_View();
		$view->assign('data', $this->data);
		$view->addScriptPath(_APP . '/emails/');
		
		// bind to this->body
		$this->body = $view->render($this->filename . '.phtml');
	}
	

	/* prepare the email Check if the Data table has a key for process_emails, 
	 * if it does we can send the email Check the env were working on, 
	 * all emails will be routed to the ini email_link email address 
	 * Send the mail if possible and then log in email_queue table */
	public function send () {

		$headers = 'From: theAudience <notifications@theaudience.com>' . "\r\n";
		$headers .= 'MIME-Version: 1.0' . "\r\n";
		$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
		$headers .= 'X-Mailer: PHP/' . phpversion();
		
		// make an array in preparation for DB storage
		$emailQueue = array('email' => $this->email_to, 'subject' => $this->subject, 'body' => $this->body, 'sent' => false);
		$dataCheck = (object) $this->db->Data->findOne(array('object_name' => 'Config', 'name' => 'process_emails'));
		
		/* if emails are enabled, then send something */
		if ($dataCheck->value == 1) {
			if (_ENV_ == 'local' || _ENV_ == 'dev') {
				$this->subject = "[Local Dev] [" . $this->email_to . "] " . $this->subject;
				$this->email_to = EMAIL_TEST;
			}
			
			if (mail($this->email_to, $this->subject, $this->body, $headers)) {
				$emailQueue['sent'] = true;
			}
		/* sending emails are disabled */
		} else {
			$emailQueue['sent'] = false;
		}
		
		
		$this->db->User_Email->insert($emailQueue);
		return true;
	}


}