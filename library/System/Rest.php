<?php 
/**
 * Master rest class
 * 
 * This file ...
 * @author Aaron Lote <aaron@theaudience.com>
 * @copyright The Audience
 * @version 2.0
 * @package master
 */

class System_Rest {

	
	const DEFAULT_METHOD = 'get';

    /* used to clean the date of bad slashes*/
    public static function cleanInput($formData) {
        foreach ($formData as $key => $value) {
            $formData[$key] = is_string($formData[$key]) ? stripslashes($formData[$key]) : $formData[$key];
        }
        return $formData;
    }


	/**
    * process request
    * @return object System_Rest_Request object
    * @access public
    * @static
    */
	public static function processRequest() {


		if (isset($_SERVER['REQUEST_METHOD'])) {
			$request_method = strtolower($_SERVER['REQUEST_METHOD']);
		} else {
			$request_method = self::DEFAULT_METHOD;
		}
		
		$return_obj	= new System_Rest_Request();
		$data		= array();

		switch ($request_method) {
			case 'get':
				$data = array();//$_GET;
			break;

			case 'post':
				$data = array();
				$temp = array();

				if (isset($_POST['_data'])) { 
					$temp = (array) json_decode($_POST['_data']);
				} else { 
					$temp = $_POST;
				}
				
				if (isset($temp['_method']) && $temp['_method'] == 'put') {
					$request_method = 'put';
					$data['put'] 	= $temp;
				} else if (isset($temp['_method']) && $temp['_method'] == 'delete') {
					$request_method = 'delete';
					$data['delete'] = $temp;
				} else {
					$data['post'] = self::cleanInput($temp);
				}
			break;

			case 'delete':
				$data = array();
				$temp = array();

				if (isset($_POST['_data'])) { 
					$temp = (array) json_decode($_POST['_data']);
				} else { 
					$temp = $_POST;
				}

				if (isset($data['_method']) && $data['_method'] == 'delete') {
					$request_method = 'delete';
					$data['delete'] = self::cleanInput($data);
				} else if (isset($data['_method']) && $data['_method'] == 'delete') {
					$request_method = 'delete';
					$data['delete'] = self::cleanInput($data);
				} else {
					$data['post'] = self::cleanInput($data);
				}
			break;

			case 'put':
				$data = array();
				$temp = array();

				if (isset($_POST['_data'])) { 
					$temp = (array) json_decode(urldecode($_POST['_data']));
				} else { 
					$temp = $_POST;
				}

				if (isset($data['_method']) && $data['_method'] == 'put') {
					$request_method = 'put';
					$data['put'] = self::cleanInput($data);
				} else {
					$data['put'] = self::cleanInput($data);
					// basically, we read a string from PHPs special input location, and then parse it out into an array via parse_str... per the PHP docs: Parses str  as if it were the query string passed via a URL and sets variables in the current scope.
					parse_str(file_get_contents('php://input'), $put_vars);
					$data = $put_vars;
				}
			break;
		}

		$data['get'] = $_GET;

		$return_obj->setMethod($request_method);
		$return_obj->setRequestVars($data);

		//dont think this ever gets used...
		if (isset($data['data'])) {
			$return_obj->setData(json_decode($data['data']));
		} else {
			$return_obj->setData($data);
		}
		return $return_obj;
	}


	/**
    * process request
    * @return object System_Rest_Request object
    * @access public
    * @static
    */
	public static function processRequestInternal($data) {

		$request_method =  (isset($data['request_method'])) ? $data['request_method'] : self::DEFAULT_METHOD;
		$return_obj		= new System_Rest_Request();
		$data			= array();

		switch ($request_method) {
			case 'get':
				$data['get'] = isset($data['get']) ? $data['get'] : array();
			break;

			case 'post':
				$data['post'] = $data['post'];
			break;

		}

		$return_obj->setMethod($request_method);
		$return_obj->setRequestVars($data);

		//dont think this ever gets used...
		if (isset($data['data'])) {
			$return_obj->setData(json_decode($data['data']));
		} else {
			$return_obj->setData($data);
		}
		return $return_obj;
	}


	public static function sendResponse($status = 200, $body = '', $content_type = 'text/html') {

		$status_header = 'HTTP/1.1 '.$status.' '.self::getStatusCodeMessage($status);
		header($status_header);
		header('Content-type: ' . $content_type);

		if ($body !== '') {
			echo $body;
			exit;
		} else { 
			$message = '';

			switch($status) {
				case 401:
					$message = 'You must be authorized to view this page.';
				break;
				case 404:
					$message = 'The requested URL ' . $_SERVER['REQUEST_URI'] . ' was not found.';
				break;
				case 500:
					$message = 'The server encountered an error processing your request.';
				break;
				case 501:
					$message = 'The requested method is not implemented.';
				break;
			}

			$signature = ($_SERVER['SERVER_SIGNATURE'] == '') ? $_SERVER['SERVER_SOFTWARE'] . ' Server at ' . $_SERVER['SERVER_NAME'] . ' Port ' . $_SERVER['SERVER_PORT'] : $_SERVER['SERVER_SIGNATURE'];
			$body = '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
						<html>
							<head>
								<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
								<title>' . $status . ' '.self::getStatusCodeMessage($status).'</title>
							</head>
							<body>
								<h1>'.self::getStatusCodeMessage($status).'</h1>
								<p>'.$message.'</p>
								<hr />
								<address>'.$signature.'</address>
							</body>
						</html>';
			echo $body;
			exit;
		}
	}


	public static function getStatusCodeMessage($status) {
		$codes = Array(
		    100 => 'Continue',
		    101 => 'Switching Protocols',
		    200 => 'OK',
		    201 => 'Created',
		    202 => 'Accepted',
		    203 => 'Non-Authoritative Information',
		    204 => 'No Content',
		    205 => 'Reset Content',
		    206 => 'Partial Content',
		    300 => 'Multiple Choices',
		    301 => 'Moved Permanently',
		    302 => 'Found',
		    303 => 'See Other',
		    304 => 'Not Modified',
		    305 => 'Use Proxy',
		    306 => '(Unused)',
		    307 => 'Temporary Redirect',
		    400 => 'Bad Request',
		    401 => 'Unauthorized',
		    402 => 'Payment Required',
		    403 => 'Forbidden',
		    404 => 'Not Found',
		    405 => 'Method Not Allowed',
		    406 => 'Not Acceptable',
		    407 => 'Proxy Authentication Required',
		    408 => 'Request Timeout',
		    409 => 'Conflict',
		    410 => 'Gone',
		    411 => 'Length Required',
		    412 => 'Precondition Failed',
		    413 => 'Request Entity Too Large',
		    414 => 'Request-URI Too Long',
		    415 => 'Unsupported Media Type',
		    416 => 'Requested Range Not Satisfiable',
		    417 => 'Expectation Failed',
		    500 => 'Internal Server Error',
		    501 => 'Not Implemented',
		    502 => 'Bad Gateway',
		    503 => 'Service Unavailable',
		    504 => 'Gateway Timeout',
		    505 => 'HTTP Version Not Supported'
		);
		return (isset($codes[$status])) ? $codes[$status] : '';
	}


}